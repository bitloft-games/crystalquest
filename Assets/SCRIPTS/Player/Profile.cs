﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Profile
{
    // Game Elements

    string playerId;
    public int TotalEnergy {
        get; set;
    }
    
    public string PlayerId
    {
        get
        {
            return playerId;
        }
    }

    public Profile(string _playerId)
    {
        playerId = _playerId;
    }


    public void ClearAllProfileData()
    {
        PlayerPrefs.DeleteAll();
    }

    public int Level
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_Level");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_Level", value);
        }
    }

    public int MaxXPLevel
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_MaxXPLevel");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_MaxXPLevel", value);
        }
    }

    public int XPLevel
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_XPLevel");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_XPLevel", value);
        }
    }

    public int MaxHPLevel
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_MaxHPLevel");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_MaxHPLevel", value);
        }
    }

    public int HPLevel
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_HPLevel");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_HPLevel", value);
        }
    }


    public int TotalCrystalsCollected
    {
        get
        {
            return PlayerPrefs.GetInt(PlayerId + "_TotalCrystalsCollected");
        }
        set
        {
            PlayerPrefs.SetInt(PlayerId + "_TotalCrystalsCollected", value);
        }
    }

    public void UpdatePlayerXP()
    {
        XPLevel++;
        XPLevel = Mathf.Clamp(XPLevel, 0, MaxXPLevel);
        ChekXPAndUpdateLevel();
    }

    public void ChekXPAndUpdateLevel()
    {
        if (XPLevel >= MaxXPLevel)
        {
            XPLevel = 0;
            Level++;
        }
    }

	public string AvatarRecipeData
	{
		get
		{
			return PlayerPrefs.GetString(PlayerId + "_AvatarRecipeData");
		}
		set
		{
			PlayerPrefs.SetString(PlayerId + "_AvatarRecipeData", value);
		}
	}

	public string AvatarColorsRecipeData
	{
		get
		{
			return PlayerPrefs.GetString(PlayerId + "_AvatarColorsRecipeData");
		}
		set
		{
			PlayerPrefs.SetString(PlayerId + "_AvatarColorsRecipeData", value);
		}
	}
}

