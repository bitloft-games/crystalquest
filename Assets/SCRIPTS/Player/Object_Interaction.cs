﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Interaction : MonoBehaviour
{
    public bool isPlayerInArea;
    public bool isObjectCollected;
    public bool IsObjectClicked;
    //	public string MsgEventName;

    void _Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 100))
            {
                Vector3 forward = transform.TransformDirection(Vector3.forward * 100);
                Debug.DrawRay(transform.position, forward, Color.magenta);
                if (hit.collider.tag == "Collectable")
                {
                    IsObjectClicked = true;
                    Destroy(gameObject);
                    Debug.Log("Destroy");
                }
            }
        }

        if (!isObjectCollected && isPlayerInArea && IsObjectClicked)
        {
            isObjectCollected = true;
            OnObjectCollectedEvent();

            Destroy(gameObject);
        }
    }

    void OnObjectCollectedEvent()
    {

        //	if (!string.IsNullOrEmpty (MsgEventName))
        //	{
        //	Messenger.Broadcast (MsgEventName);
        //	}

    }

    void OnPlayerInActiveArea()
    {

        if (!isPlayerInArea)
        {
            isPlayerInArea = true;
            Debug.Log(isPlayerInArea);
            InvokeRepeating("_Update", 0.1f, 0.1f);
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnPlayerInActiveArea();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnPlayerInActiveArea();
        }
    }

    void OnTriggerExit(Collider other)
    {
        isPlayerInArea = false;
        Debug.Log(isPlayerInArea);
        CancelInvoke("_Update");
    }
}
