﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PlayerNavigationPointer : MonoBehaviour
{


    public NavMeshAgent agent;
    GameObject curentTarget;
    void Start()
    {
        //		agent = transform.GetComponent<NavMeshAgent> ();

        Messenger.AddListener<GameObject>("SetPlayerNavigationPointerTarget", SetPlayerNavigationPointerTarget);

        InvokeRepeating("updateDestination", 0.02f, 0.01f);

        //		agent.updateRotation = true;
    }

    public void SetPlayerNavigationPointerTarget(GameObject target)
    {

        curentTarget = target;

        //		agent.destination = target.transform.position;
        //
        //		agent.SetDestination (target.transform.position);

        Debug.Log("PlayerNavigationPointer Destination set to " + target.name);
    }

    void updateDestination()
    {
        if (curentTarget != null)
        {
            transform.LookAt(curentTarget.transform.position);
            //			agent.destination = curentTarget.transform.position;
            //			agent.SetDestination (curentTarget.transform.position);
        }
    }

}
