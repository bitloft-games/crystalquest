﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Lean.Touch;


[RequireComponent(typeof(PlayerControllerSilver))]
public class PlayerInput : MonoBehaviour
{

	// Event signature
	[System.Serializable] public class LeanFingerEvent : UnityEvent<LeanFinger> {}

	[Tooltip("If the finger is over the GUI, ignore it?")]
	public bool IgnoreIfOverGui;

	[Tooltip("If the finger started over the GUI, ignore it?")]
	public bool IgnoreIfStartedOverGui;

	[Tooltip("How many times must this finger tap before OnFingerTap gets called? (0 = every time)")]
	public int RequiredTapCount = 0;

	[Tooltip("How many times repeating must this finger tap before OnFingerTap gets called? (e.g. 2 = 2, 4, 6, 8, etc) (0 = every time)")]
	public int RequiredTapInterval;

	public LeanFingerEvent OnFingerTap;

    PlayerControllerSilver playerController;

	public LayerMask ignoreLayers;

    private void Start()
    {
        playerController = GetComponent<PlayerControllerSilver>();
    }


	protected virtual void OnEnable()
	{
		// Hook events
		LeanTouch.OnFingerTap += FingerTap;

	}

	protected virtual void OnDisable()
	{
		// Unhook events
		LeanTouch.OnFingerTap -= FingerTap;

	}

	private void FingerTap(LeanFinger finger)
	{
		// Ignore?
		if (IgnoreIfOverGui == true && finger.IsOverGui == true)
		{
			return;
		}

		if (IgnoreIfStartedOverGui == true && finger.StartedOverGui == true)
		{
			return;
		}

		if (RequiredTapCount > 0 && finger.TapCount != RequiredTapCount)
		{
			return;
		}

		if (RequiredTapInterval > 0 && (finger.TapCount % RequiredTapInterval) != 0)
		{
			return;
		}

		// Call event
		OnFingerTap.Invoke(finger);
	}


	public void OnTap()
	{
		
			HandleClick();		   

	}

	float downDelta;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (CanvasManager.Instance.currentScreen == CANVAS_SCREEN.DEBUG)
            {
                CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.PLAYER_HUD);
            }
            else
            {
                CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.DEBUG);
            }
        }

        if (Input.GetMouseButton(0))
        {
            downDelta += Time.deltaTime;
            if (downDelta > 0.1f)
                CameraManager.Instance.currentMode = CameraManager.Mode.Looking;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            downDelta = 0f; // reset since we stopped holding the button down
        }
    }

    private void HandleClick()
    {
        Ray ray = CameraManager.Instance.CameraRay(Input.mousePosition);
        RaycastHit hit;
       
		if (Physics.Raycast (ray, out hit, 200f, ~ignoreLayers))
		{
			if (hit.collider.tag == "Ground")
			{
				playerController.SetTargetPosition (hit.point);
				CameraManager.Instance.currentMode = CameraManager.Mode.Follow;
			} else
			{
				ClickTarget ct = hit.collider.GetComponent<ClickTarget>();
				if (ct != null)
				{
					float distance = (hit.point - transform.position).magnitude;
					if (distance - 1 <= ct.Range) // it would be more accurate to use player stopping distance
					{
						ct.OnClick ();
					} else
					{ // take the player in the direction of the target but just to the limit of the minimum range to fire at target
						var heading = hit.point - transform.position;
						var goal = hit.point - (heading.normalized * ct.Range);
						playerController.SetTargetPosition (goal);
						CameraManager.Instance.currentMode = CameraManager.Mode.Follow;
					}
					// tell game controller to pick up crystal
					// game controller logs the event
					// quest conditions are checked
				}
			}
		} 
    }
}