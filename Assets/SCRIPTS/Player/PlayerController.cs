﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;
using UMA.CharacterSystem;

public class PlayerController : MonoBehaviour
{

    private static PlayerController _instance = null;

    public static PlayerController SharedInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType(typeof(PlayerController)) as PlayerController;
            }

            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    //Variables
    public GameObject particle;
    public UnityEngine.AI.NavMeshAgent agent;
    public Animator animator;
    protected Object particleClone;

    public bool isPlayerInCrystalArea;
    public bool isPlayerInEnemyArea;
    public bool isCrystalSelected;
    public bool isPlayerTurnToAttack;

    public Transform EffectHitPoint;

    public PLAYER_STATE currentState;

    public Transform CameraObjects;
    public GameObject playerShield;

    public float health;
    public float maxHealth;

    public Transform PlayerNavigationPointer;
    public Transform PointerImage;

    public LayerMask ignoreLayers;

	public DynamicCharacterAvatar Avatar;

    void Start()
    {
		Avatar = GetComponent<DynamicCharacterAvatar> ();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();//get Navigation mesh component on player
        agent.updateRotation = false;
        animator = GetComponent<Animator>();//get animator component on player
        particleClone = null;

        maxHealth = 100;
        health = maxHealth;
 
		string recipeData = GameManager.Instance.playerProfile.AvatarRecipeData;
		if (!string.IsNullOrEmpty (recipeData))
		{
			Avatar.LoadFromRecipeString (recipeData);
		}
    }

    public void SetCurrentStateTo(PLAYER_STATE state)//Player States
    {

        currentState = state;
        switch (currentState)
        {
            case PLAYER_STATE.IDLE:
                if (agent)
                {
                    agent.enabled = true;

                }
                animator.SetFloat("Speed", 0.0f);
                break;

            case PLAYER_STATE.WALK:
                break;

            case PLAYER_STATE.ATTACK://set the isAttack flag true to play the ATTACK animation
                animator.SetBool(Animator_HashIds.Instance.isAttack, true);
                break;

            case PLAYER_STATE.SHIELD_ATTACK:
                animator.SetBool(Animator_HashIds.Instance.isShield, true);//set the isShield flag true to play the SHIELD_ATTACK animation
                break;

            case PLAYER_STATE.HURT://set the isHurt flag true to play the HURT animation
                animator.SetBool(Animator_HashIds.Instance.isHurt, true);
                break;

            case PLAYER_STATE.LEVEL_UP://set the isLevelUP flag true to play the LEVEL_UP animation
                animator.SetBool(Animator_HashIds.Instance.isLevelUP, true);
                break;

            case PLAYER_STATE.NOT_HAPPY://set the isNotHappy flag true to play the NOT_HAPPY animation
                animator.SetBool(Animator_HashIds.Instance.isNotHappy, true);
                break;

            case PLAYER_STATE.CRYSTAL_COLLECTED:
                animator.SetBool(Animator_HashIds.Instance.isCrystalCollected, true);//set the isCrystalCollected flag true to play the CRYSTAL_COLLECTED animation
                //CameraController.SharedInstance.SetCameraToThirdPersonCrystalCollectedView();//set Camera to ThirdPersonCrystalCollected to view animation 

                break;

            case PLAYER_STATE.BATTLE_WIN:
                animator.SetBool(Animator_HashIds.Instance.isBattleWin, true);//set the isBattleWin flag true to play the BATTLE_WIN animation
                //CameraController.SharedInstance.SetCameraToBattleWinLooseView();//set Camera to BattleWin to view animation 

                health = maxHealth;
                GameManager.Instance.playerProfile.MaxXPLevel = 10;
                GameManager.Instance.playerProfile.MaxHPLevel = 10;
                GameManager.Instance.playerProfile.HPLevel = GameManager.Instance.playerProfile.MaxHPLevel;


                break;

            case PLAYER_STATE.BATTLE_LOSE:
                animator.SetBool(Animator_HashIds.Instance.isBattleLose, true);//set the isBattleLose flag true to play the BATTLE_LOSE animation
                //CameraController.SharedInstance.SetCameraToBattleWinLooseView();//set Camera to BattleLose to view animation 

                health = maxHealth;
                GameManager.Instance.playerProfile.MaxXPLevel = 10;
                GameManager.Instance.playerProfile.MaxHPLevel = 10;
                GameManager.Instance.playerProfile.HPLevel = GameManager.Instance.playerProfile.MaxHPLevel;
                break;

            case PLAYER_STATE.CRYSTAL_COLLECT_FAIL://when Player fails to collect crystal
                animator.SetBool(Animator_HashIds.Instance.isCrystalCollectFailed, true);

                break;

        

        }
    }

    public void SetDestination()//set Destination Target for Player
    {
        if (!agent.enabled)
        {
            return;
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 1000, ~ignoreLayers))

        {
            if (hit.collider.tag == "Crystal" && isPlayerInCrystalArea)
            {
                return;
            }

            if (particleClone != null)
            {
                GameObject.Destroy(particleClone);
                agent.updateRotation = false;
                particleClone = null;
            }

            Vector3 targetPosition = Utilities.GetRandomPointOnNavMesh(hit.point, 2.0f);
            particleClone = Instantiate(particle, targetPosition, particle.transform.rotation);//particle effect on the clicked position
            agent.SetDestination(targetPosition);
        }
    }

    protected void SetupAgentLocomotion()//set the movement towards TargetDestination
    {
        if (AgentDone() )
        {
            animator.SetFloat("Speed", 0.0f);
            if (particleClone != null)
            {
                GameObject.Destroy(particleClone);
                animator.SetBool("DoubleTab", false);//Enable doubletab and sets the Player to run state 
                particleClone = null;
            }
        }
        else
        {
            agent.updateRotation = true;
            float speed = agent.desiredVelocity.magnitude;
            Vector3 velocity = Quaternion.Inverse(transform.rotation) * agent.desiredVelocity;
            float angle = Mathf.Atan2(velocity.x, velocity.z) * 180.0f / 3.14159f;
            animator.SetFloat("Speed", speed);
        }
    }

    void OnAnimatorMove()
    {
        if (Time.timeScale == 1.0f)
        {
            if (agent.enabled)
            {

                if (Time.deltaTime >= 0.01f)
                {
                    agent.velocity = animator.deltaPosition / Time.deltaTime;
                }

            }
            else
            {
                transform.position += animator.deltaPosition;
                transform.rotation = animator.rootRotation;
            }
        }
    }

    protected bool AgentDone()
    {
        return !agent.pathPending && AgentStopping();
    }

    protected bool AgentStopping()
    {
        return agent.remainingDistance <= agent.stoppingDistance;
    }

    void Update()
    {
        if (agent.enabled)
        {
            SetupAgentLocomotion();
        }
        else
        {
            if (particleClone != null)
            {
                GameObject.Destroy(particleClone);
            }
        }
      
    }



}
