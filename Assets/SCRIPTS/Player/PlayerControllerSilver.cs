﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UMA.CharacterSystem;

[RequireComponent (typeof(NavMeshAgent))]
[RequireComponent (typeof(Animator))]

public class PlayerControllerSilver : MonoBehaviour
{
	private Vector3 targetPosition;
	NavMeshAgent agent;
	Animator anim;
	bool run = false;
	bool shouldMove;

	Vector2 smoothDeltaPosition = Vector2.zero;
	Vector2 velocity = Vector2.zero;

	public DynamicCharacterAvatar Avatar;


	// Use this for initialization
	void Start ()
	{
		agent = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		agent.updatePosition = false;

		Messenger.AddListener (GAME_EVENTS.SET_PLAYER_AVATAR.ToString (), SetPlayerAvatar);

		Avatar = GetComponent<DynamicCharacterAvatar> ();

		SetPlayerAvatar ();


	}
    
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.N))
		{
			anim.SetBool ("isAttack", true);
		}
		
		Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

		// Map 'worldDeltaPosition' to local space
		float dx = Vector3.Dot (transform.right, worldDeltaPosition);
		float dy = Vector3.Dot (transform.forward, worldDeltaPosition);
		Vector2 deltaPosition = new Vector2 (dx, dy);

		// Low-pass filter the deltaMove
		float smooth = Mathf.Min (1.0f, Time.deltaTime / 0.15f);
		smoothDeltaPosition = Vector2.Lerp (smoothDeltaPosition, deltaPosition, smooth);

		// Update velocity if time advances
		//if (Time.deltaTime > 1e-5f) velocity = smoothDeltaPosition / Time.deltaTime;

		bool previous = shouldMove;
		shouldMove = agent.remainingDistance - agent.stoppingDistance > agent.radius; 

		if (!shouldMove)
		{
			if (previous && !shouldMove)
			{
				anim.SetBool ("run", false);
				run = false;
			}

			anim.SetBool ("move", false);
		} else
		{
			anim.SetBool ("move", true);
		}

		// Update animation parameters
		anim.SetFloat ("Speed", velocity.magnitude);
		//anim.SetFloat("Speed", velocity.x);
		//anim.SetFloat("vely", velocity.y);

	}

	public void SetTargetPosition (Vector3 targetPosition)
	{
		anim.SetBool ("move", true);
		if ((targetPosition - transform.position).magnitude > 4)
		{
			anim.SetBool ("run", true);
			agent.speed = 4f;
		} else
		{
			anim.SetBool ("run", false);
			agent.speed = 2f;
		}
		agent.SetDestination (targetPosition);
	}

	private void OnAnimatorMove ()
	{
		// Update position to agent position
		transform.position = agent.nextPosition;
	}

	public void SetPlayerAvatar(){

		GameManager.Instance.LoadPlayerAvatar(Avatar);
	}


}
