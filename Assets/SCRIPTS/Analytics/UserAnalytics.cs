﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class UserAnalytics : SingletonManager<UserAnalytics> {

    private int correctAnswers;
    private int incorrectAnswers;
    private int aliensDefeated;
    private bool bossDefeated;
    private string finalGrade;
    
    void Start()
    {
        Analytics.SetUserId(System.Guid.NewGuid().ToString());
        correctAnswers = 0;
        incorrectAnswers = 0;
        aliensDefeated = 0;
        bossDefeated = false;
        finalGrade = "";
    }

    public void AnsweredIncorrectly(string questionID)
    {
        Answered(questionID, false);
        incorrectAnswers++;
    }

    public void AnsweredCorrectly(string questionID)
    {
        Answered(questionID, true);
        correctAnswers++;
    }

    public void SetGrade(string grade)
    {
        finalGrade = grade;
    }

    private void Answered(string questionID, bool answeredCorrectly)
    {
        Analytics.CustomEvent("CrystalQuestion", new Dictionary<string, object>
        {
            { "questionID", questionID },
            { "answeredCorrectly", answeredCorrectly }
        });
    }

    public void CrystalCollected(string crystalName)
    {
        Analytics.CustomEvent("CrystalCollected", new Dictionary<string, object>
        {
            { "name", crystalName }
        });
    }

    public void AlienDefeated() {
        Analytics.CustomEvent("AlienDefeated");
        aliensDefeated++;
    }

    public void AlienBossDefeated() {
        Analytics.CustomEvent("AlienBossDefeated", new Dictionary<string, object>
        {
            { "time", Time.timeSinceLevelLoad.ToString() }
        });
    }

    void OnApplicationQuit()
    {
        Analytics.CustomEvent("ApplicationQuit", new Dictionary<string, object>
        {
            { "CorrectAnswers", correctAnswers },
            { "IncorrectAnswers", incorrectAnswers },
            { "AliensDefeated", aliensDefeated },
            { "BossDefeated", bossDefeated },
            { "FinalGrade", finalGrade },
            { "TimeSinceStartingLevel", Time.timeSinceLevelLoad.ToString() }
        });
    }
}
