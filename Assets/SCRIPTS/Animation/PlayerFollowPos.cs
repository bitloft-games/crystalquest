﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowPos : MonoBehaviour
{
    public Transform target;
    // Use this for initialization
    void Start()
    {
        target = PlayerController.SharedInstance.transform;
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {

        if (PlayerController.SharedInstance.currentState != PLAYER_STATE.ATTACK)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * 4.0f);
            transform.rotation = PlayerController.SharedInstance.transform.rotation;
        }
    }
}
