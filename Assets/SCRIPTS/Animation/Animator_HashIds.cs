using UnityEngine;
using System.Collections;

public class Animator_HashIds : SingletonManager<Animator_HashIds>
{
    
    // Here we store the hash Ids for various strings used in our animators.
    public int Layer_Base_index;
    public int DoubleTab;
    public int Speed;
    public int Direction;
    public int IsGroundActive;
    public int isAttack;
    public int isShield;
    public int isHurt;
    public int isHappy;
    public int isLevelUP;
    public int isNotHappy;
    public int isCrystalCollected;
    public int isBattleWin;
    public int isBattleLose;
    public int isCrystalCollectFailed;
    public int crystalCollectType;
    public int isLookAtHologram;
    public int isLookAtWrist;

    void Start()
    {

        Layer_Base_index = 0;

        DoubleTab = Animator.StringToHash("DoubleTab");
        Speed = Animator.StringToHash("Speed");
        Direction = Animator.StringToHash("Direction");
        IsGroundActive = Animator.StringToHash("IsGroundActive");
        isAttack = Animator.StringToHash("isAttack");
        isShield = Animator.StringToHash("isShield");
        isHurt = Animator.StringToHash("isHurt");
        isHappy = Animator.StringToHash("isHappy");
        isLevelUP = Animator.StringToHash("isLevelUP");
        isNotHappy = Animator.StringToHash("isNotHappy");
        isCrystalCollected = Animator.StringToHash("isCrystalCollected");
        isBattleWin = Animator.StringToHash("isBattleWin");
        isBattleLose = Animator.StringToHash("isBattleLose");
        isCrystalCollectFailed = Animator.StringToHash("isCrystalCollectFailed");
        crystalCollectType = Animator.StringToHash("crystalCollectType");
        isLookAtHologram = Animator.StringToHash("isLookAtHologram");
        isLookAtWrist = Animator.StringToHash("isLookAtWrist");
    }
}