﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUI : DynamicUI {

    public GameObject questionPrefab;
    public GameObject choicePrefab;

    private QuizQuestion currentQuestion;

    public void DisplayQuestion(QuizQuestion question)
    {
        currentQuestion = question;
        UpdateText();
    }

    protected override void CreateChildren()
    {
        // Quick fix for some questions that are bad
        // The questions themselves should be caught and fixed
        // before they get here.

        if (currentQuestion.body.value != null)
        {
            // add question text
            GameObject gotc = createTextChild(questionPrefab, currentQuestion.body.value);
            gotc.transform.SetParent(transform, false);

            //add choices
            ToggleGroup choiceGroup = gameObject.GetComponent<ToggleGroup>();
            for (int i = 0; i < currentQuestion.interaction.choice.Count; i++)
            {
                GameObject goctc = createToggleChild(currentQuestion.interaction.choice[i], choiceGroup);
                goctc.transform.SetParent(transform, false);
            }
        }
    }

    protected GameObject createToggleChild(QuizQuestion.Choice choice, ToggleGroup choiceGroup)
    {
        GameObject choiceObject = Instantiate(choicePrefab) as GameObject;

        QuizChoice quizChoice = choiceObject.GetComponent<QuizChoice>();
        quizChoice.SetChoice(choice);

        Text textComponent;
        string choiceText = choice.body.value;
        textComponent = choiceObject.GetComponent<Text>();
        if (textComponent == null) textComponent = choiceObject.GetComponentInChildren<Text>();
        textComponent.supportRichText = true;
        textComponent.text = choiceText;

        Toggle toggle = choiceObject.GetComponent<Toggle>();
        toggle.group = choiceGroup;

        SetDynamicTextHeight(textComponent, choiceText.Trim());
        
        return choiceObject;
    }
}
