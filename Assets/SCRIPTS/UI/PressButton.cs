﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressButton : Button {

    public void Update()
    {
        if (IsPressed())
        {
            this.onClick.Invoke();
        }
    }
}
