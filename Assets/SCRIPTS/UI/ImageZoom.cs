﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ImageZoom : MonoBehaviour, IPointerClickHandler {
    
    private Image image;

    void Start ()
    {
        image = GetComponent<Image>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CanvasUI_ImageZoom.instance.zoomIn(image);
    }
}
