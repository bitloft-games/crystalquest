﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(VerticalLayoutGroup))]
[RequireComponent(typeof(RectTransform))]
public abstract class DynamicUI : MonoBehaviour {

    protected float desiredUIWidth;
    protected VerticalLayoutGroup vlGroup;
    protected RectTransform rectTransform;
    
    protected abstract void CreateChildren();

    public void UpdateText()
    {
        clearChildren();
        setDesiredWidth();
        CreateChildren();
    }

    void Awake()
    {
        vlGroup = GetComponent<VerticalLayoutGroup>();
        rectTransform = GetComponent<RectTransform>();
    }

    protected void setDesiredWidth()
    {
        desiredUIWidth = rectTransform.rect.width - (vlGroup.padding.left + vlGroup.padding.right);
    }

    protected void SetDynamicTextHeight(Text text, string myString)
    {
        // This is the height that the text would fit at the current font height setting (see the inspector)
        TextGenerator textGen = new TextGenerator();
        TextGenerationSettings generationSettings = text.GetGenerationSettings(new Vector2(desiredUIWidth, 9999));

        float height = textGen.GetPreferredHeight(myString, generationSettings);

        // Resize the Rect to the size of your text
        RectTransform rt = text.gameObject.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(800, height);
    }

    public void clearChildren()
    {
        // Can't just delete using one loop because 
        // destroyImmediate (which is required to make any changes in edit mode)
        // effects GetChild

        int children = transform.childCount;
        GameObject[] obs = new GameObject[children];
        for (int i = 0; i < children; i++)
        {
            obs[i] = transform.GetChild(i).gameObject;
        }
        for (int i = 0; i < children; i++)
        {
            GameObject.DestroyImmediate(obs[i]);
        }
    }

    protected GameObject createTextChild(GameObject textPrefab, string content)
    {
        GameObject textObject = Instantiate(textPrefab) as GameObject;
        Text textComponent;

        textComponent = textObject.GetComponent<Text>();
        if (textComponent == null) textComponent = textObject.GetComponentInChildren<Text>();
        textComponent.supportRichText = true;
        textComponent.text = content;
        SetDynamicTextHeight(textComponent, content.Trim());
        
        return textObject;
    }

    protected GameObject createIMGChild(GameObject imgPrefab, string image)
    {
        GameObject imgObject = Instantiate(imgPrefab) as GameObject;
        Image imgComponent = imgObject.GetComponentInChildren<Image>();

        Sprite imgSprite = ResourceManager.LoadSpritefromImage(image);
        if (imgSprite != null) imgComponent.sprite = imgSprite;
        else
        {
            Debug.Log("Could not find texture " + image);
            // return the default imageobject that should show a blank image
            // That way we'll know something's wrong.
            return imgObject;
        }
        Rect imageRect = imgComponent.sprite.textureRect;
        
        //no stretching textures up, use desired width
        float targetWidth;
        float targetHeight;
        if (imageRect.width > desiredUIWidth)
        {
            targetHeight = (desiredUIWidth * imageRect.height) / imageRect.width;
            targetWidth = desiredUIWidth;
        }
        else
        {
            targetHeight = imageRect.height;
            targetWidth = imageRect.width;
        }
        RectTransform rt = imgObject.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(targetWidth, targetHeight);

        return imgObject;
    }

}
