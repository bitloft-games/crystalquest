﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_GoalsList : MonoBehaviour
{

	public ScrollRect scrollRect_InActiveGoalsList;
	public ScrollRect scrollRect_ActiveGoalsList;
	public ScrollRect scrollRect_AchievedGoalsList;

	public GameObject Prefab_CanvasUI_GoalsList_Item;
	// Use this for initialization
	void Start ()
	{
		if (GoalsManager.Instance == null)
		{
			return;
		}

		foreach (var key in GoalsManager.Instance.InActiveGoalsList)
		{
			AddGoalToList (GoalsManager.Instance.GoalsData [key], scrollRect_InActiveGoalsList);
		}

		foreach (var key in GoalsManager.Instance.ActiveGoalsList)
		{
			AddGoalToList (GoalsManager.Instance.GoalsData [key], scrollRect_ActiveGoalsList);
		}

		foreach (var key in GoalsManager.Instance.AchievedGoalsList)
		{
			AddGoalToList (GoalsManager.Instance.GoalsData [key], scrollRect_AchievedGoalsList);
		}

	}

	void AddGoalToList (Goal goal, ScrollRect scrollRect)
	{
		
		GameObject item = Instantiate (ResourceManager.LoadCanvasObject ("CanvasUI_GoalsList_Item")) as GameObject;

		item.transform.SetParent (scrollRect.content);

		item.GetComponent<CanvasUI_GoalsList_Item> ().SetDetails (goal);
	}

	public void OnCloseButtonClickevent ()
	{		
		CanvasManager.Instance.SetCurrentScreenTo (CanvasManager.Instance.previousScreen);
	}
}
