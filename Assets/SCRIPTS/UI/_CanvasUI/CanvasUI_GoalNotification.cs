﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanvasUI_GoalNotification : MonoBehaviour {

	public Text Text_GoalName;
	public Text Text_Description;
	public Text Text_Message;

	// Use this for initialization
	void Start () {
		
	}
	
	public void SetDetails(Goal goal){
		
		Text_Description.text = goal.Description;
		Text_Message.text = goal.Message;
	}

	public void OnAnimationCompleteEvent(){
		
		Destroy (gameObject);
	}
}
