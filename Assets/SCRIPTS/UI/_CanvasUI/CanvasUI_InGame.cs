﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_InGame : MonoBehaviour
{
    public Text EnergyValue;

    void Start()
    {
        Messenger.AddListener("OnPlayerDataUpdated", updatePlayerData);
        updatePlayerData();
    }

    void Awake()
    {
        updatePlayerData();
    }

    void updatePlayerData()
    {
        if (EnergyValue != null) EnergyValue.text = GameManager.Instance.GetEnergy().ToString();
    }

    public void OnShowInventoryEvent()
    {
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.INVENTORY);
    }

	public void OnCharacterCustomButtonClickEvent(){
		CanvasManager.Instance.SetCurrentScreenTo (CANVAS_SCREEN.CHARACTER_CUSTOMIZATION);
	}

	public void OnGoalsButtonClickEvent(){
		CanvasManager.Instance.SetCurrentScreenTo (CANVAS_SCREEN.GOALS_SCREEN);
	}

	public void OnQuitButtonClickEvent(){
		Application.Quit ();
	}
}
