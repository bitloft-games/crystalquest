﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_Quiz: MonoBehaviour {

    public QuizUI quizUI;
    public Text indexText;
    public Button nextButton;
    public Button submitButton;
    public Button previousButton;
    public Button continueButton;
    public Text gradeText;
    public Text resultsText;
    
    protected int questionIndex;

    void Start()
    {
        // Deactivate things we don't need to start with
        previousButton.gameObject.SetActive(false);
        submitButton.gameObject.SetActive(false);
        gradeText.gameObject.SetActive(false);
        resultsText.gameObject.SetActive(false);
        continueButton.gameObject.SetActive(false);

        questionIndex = 0;
        quizUI.DisplayQuestion(LMSTool.Instance.questions[questionIndex]);
        ShowIndex();
    }

    public void NextQuestion()
    {
        questionIndex++;
        quizUI.DisplayQuestion(LMSTool.Instance.questions[questionIndex]);
        ShowIndex();
        if (questionIndex + 1 >= LMSTool.Instance.questions.Count)
        {
            nextButton.gameObject.SetActive(false);
            submitButton.gameObject.SetActive(true);
        }
        if (questionIndex > 0) previousButton.gameObject.SetActive(true);
    }

    public void PreviousQuestion()
    {
        questionIndex--;
        quizUI.DisplayQuestion(LMSTool.Instance.questions[questionIndex]);
        ShowIndex();
        if (questionIndex - 1 < 0)
            previousButton.gameObject.SetActive(false);
        if (questionIndex < LMSTool.Instance.questions.Count)
        {
            nextButton.gameObject.SetActive(true);
            submitButton.gameObject.SetActive(false);
        }
            
    }

    private void ShowIndex()
    {
        if (indexText != null)
            indexText.text = "Question #" + (questionIndex + 1).ToString();
    }

    public void SubmitAnswers()
    {
        LMSTool.Instance.SaveAttemptAnswers().Then(() =>
        {
            DisplayGrade();
        });
    }

    public void DisplayGrade()
    {
        LMSTool.Instance.GetGrade().Then((grade) =>
        {
            previousButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
            submitButton.gameObject.SetActive(false);
            quizUI.gameObject.SetActive(false);
            indexText.gameObject.SetActive(false);
            
            gradeText.text = grade.letter;
            resultsText.text = grade.rawachieved + "/" + grade.rawpossible + " points";

            gradeText.gameObject.SetActive(true);
            resultsText.gameObject.SetActive(true);
            continueButton.gameObject.SetActive(true);
        });
        
    }
    
    public void OnCloseButtonClickEvent()
    {
        Messenger.Broadcast("CloseCurrentScreen");
        CameraManager.Instance.SetCameraMode(CameraManager.Mode.Follow);
    }
}
