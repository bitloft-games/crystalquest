﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_GoalsList_Item : MonoBehaviour {

	public Text Text_GoalName;
	public Text Text_Description;
//	public Text Text_Message;

	// Use this for initialization
	void Start () {

	}

	public void SetDetails(Goal goal){

		Text_GoalName.text = goal.GoalID.ToString ();
		Text_Description.text = goal.Description;
		//Text_Message.text = goal.Message;
	}
}
