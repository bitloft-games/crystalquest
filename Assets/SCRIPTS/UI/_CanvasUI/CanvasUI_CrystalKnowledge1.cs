﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using PowerUI;

public class CanvasUI_CrystalKnowledge1 : MonoBehaviour
{
    public Text Crystal_Title;
    public GameObject ActivateButton;
    public GameObject CloseButton;
    public GameObject NextButton;
    int index;

    public Sprite HP_Level;
    public Sprite XP_Level;
    public Sprite Blank_Level;
    public Text Player_Level_Text;
    public Image[] XP = new Image[10];
    public Image[] HP = new Image[10];

    //public HtmlUIPanel HtmlContentFile;
    public Animator MenuController;
    public Image Crystal_Image;
    void Start()
    {
        
        CameraManager.Instance.currentMode = CameraManager.Mode.UI;
        SoundManager.Instance.PlaySound(SOUND_CLIPS.OPENCLOSE, CanvasManager.Instance.transform);
        
        //crystal_Image color acc. to crystal ID
      //  Crystal_Image.color = CrystalManager.SharedInstance.Crystal_Color[CrystalManager.SharedInstance.currentCrystalCard.id];

        //assign text file with knowledge content acc. to id
        TextAsset knowledgeTextAsset = Resources.Load("crystals/" + CrystalManager.Instance.currentCrystalCard.id+"_html") as TextAsset;
        KnowledgeUI kui = this.GetComponentInChildren<KnowledgeUI>();
        kui.KnowledgeTextAsset = knowledgeTextAsset;
        kui.UpdateText();

        ActivateButton.SetActive(false);//Disable Activate button
        CloseButton.SetActive(true);//Enable close button
        GameManager.Instance.SetTimeScale(0.0f);
       
        ChangeLevelBar(XP, GameManager.Instance.playerProfile.XPLevel, GameManager.Instance.playerProfile.MaxXPLevel, XP_Level, Blank_Level);//Update XP LevelBar for player
        ChangeLevelBar(HP, GameManager.Instance.playerProfile.HPLevel, GameManager.Instance.playerProfile.MaxHPLevel, HP_Level, Blank_Level);//Update HP LevelBar for player
        Player_Level_Text.text = "Lv" + GameManager.Instance.playerProfile.Level;//Update Player level
        ShowKnowledgeText();
    
        MenuController.Play("CanvasUI_CrystalKnowledgeOpen");//open animation
    }

    public void OnNextButtonClickEvent()
    {
        Destroy(gameObject);
    }
    
    void Update()
    {
        //Invoke("ChangeColour",.001f);
        ChangeColour();
    }

    void ChangeLevelBar(Image[] XP, int level, int maxLevel, Sprite activeSprite, Sprite deactiveSprite)//Change levelBar Display Images acc. to Player XP and HP
    {
        Image img = null;
        for (int i = 0; i < maxLevel; i++)
        {
            if (XP[i] != null)
            {
                img = XP[i].GetComponent<Image>();
            }
            if (img != null)
            {
                img.sprite = i < level ? activeSprite : deactiveSprite;// activeSprite to fill the LevelBar ,deactiveSprite to empty the LevelBar
            }
        }
    }

    void ChangeColour()//Update color change
    {
        //PlayerController.SharedInstance.UpdateUIColorMode();
    }

    void ShowKnowledgeText()
    {
        Crystal_Title.text = "" + CrystalManager.Instance.currentCrystalCard.title;//crystal card title 
    }

    public void OnCloseButtonClickEvent()
    {
        GetComponent<Animator>().Play("CanvasUI_CrystalKnowledgeClose");//close animation
        Invoke("ModeChange", .9f);
        Invoke("Destroy", .9f);

        GameManager.Instance.SetCurrentStateTo(GAME_STATE.IN_GAME);
        SoundManager.Instance.PlaySound(SOUND_CLIPS.OPENCLOSE, CanvasManager.Instance.transform);
        GameManager.Instance.SetTimeScale(1.0f);
    }

    public void OnPracticeEnergyEvent()
    {
        GameManager.Instance.IncreaseEnergy(1);
        OnCloseButtonClickEvent();
    }

    void Destroy()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        if (GameManager.Instance)
        {
            GameManager.Instance.SetTimeScale(1.0f);
        }
    }
}
