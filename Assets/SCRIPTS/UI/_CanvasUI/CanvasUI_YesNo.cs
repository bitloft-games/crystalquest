﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CanvasUI_YesNo : MonoBehaviour {

    public Text questionText;
    public Button yesButton;
    public Text yesButtonText;
    public Button noButton;

    private UnityAction yesAction;
    private UnityAction noAction;
    
    public void Setup(string question, string yesText, UnityAction yesAction, string noText, UnityAction noAction = null)
    {
        this.yesAction = yesAction;
        this.noAction = noAction;
        yesButtonText.text = yesText;
        yesButton.onClick.AddListener(onYes);
        noButton.onClick.AddListener(onNo);

        questionText.text = question;
    }

    public void onYes()
    {
        yesAction.Invoke();
        OnClose();
    }

    public void onNo() {
        if (noAction != null) noAction.Invoke();
        OnClose();
    }

    public void OnClose()
    {
        GameManager.Instance.SetCurrentStateTo(GAME_STATE.IN_GAME);
        SoundManager.Instance.PlaySound(SOUND_CLIPS.OPENCLOSE, CanvasManager.Instance.transform);
        GameManager.Instance.SetTimeScale(1.0f);
    }

}
