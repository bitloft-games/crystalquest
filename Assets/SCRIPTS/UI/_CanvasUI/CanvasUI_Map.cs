﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasUI_Map : MonoBehaviour {

	public void OnCloseButtonClickEvent()
    {
        Messenger.Broadcast("CloseCurrentScreen");
    }

    public void GoToIsland(string island)
    {
        Scene newScene = SceneManager.GetSceneByName(island);
        if (newScene != null)
        {
            if (!newScene.isLoaded)
                GameManager.Instance.LoadSceneAndSetCurrentStateTO(island, GAME_STATE.IN_GAME, LoadSceneMode.Single);
        }
        else Debug.Log("No scene named " + island + " is available, maybe it's a typo?");
    }
}
