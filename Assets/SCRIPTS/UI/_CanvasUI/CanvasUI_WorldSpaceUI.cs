﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasUI_WorldSpaceUI : MonoBehaviour
{

    public Camera target;
    // Use this for initialization
    void Start()
    {
        target = Camera.main;
        GetComponent<Canvas>().worldCamera = target;
    }

    // Update is called once per frame
    void Update()
    {
        //	eulerAngles = transform.eulerAngles;
        transform.LookAt(target.transform.position);
        //	eulerAngles.y = transform.eulerAngles.y;
        //	transform.eulerAngles = eulerAngles; 
    }
}
