﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_Inventory : MonoBehaviour
{
    public Text NoOfCrystals_text;
    public Animator MenuController;
    public Transform CrystalCardLayout;
    public GameObject CrystalCard;

    void Start()
    {
        foreach( Crystal crystal in  CrystalManager.Instance.CrystalDictionary.Values)//for number of crystals present in environment
        {
            // create a new card
            GameObject cardPrefab = Instantiate(CrystalCard);
            cardPrefab.transform.SetParent(CrystalCardLayout);

            cardPrefab.GetComponent<CrystalCard_Script>().SetCardDetails(crystal);
        }
    }

    void Update()
    {   //crystals collected-total crystals  count
        NoOfCrystals_text.text = CrystalManager.Instance.CrystalsFound() + "/" + CrystalManager.Instance.CrystalDictionary.Count;
    }
    
    public void OnCloseButtonClickEvent()
    {
        Invoke("CanvasChange", 0.9f);//after animation complete
    }

    void CanvasChange()//Back to Ingame
    {
        Destroy(gameObject);
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.PLAYER_HUD);

    }
}
