﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CanvasUI_ImageZoom : MonoBehaviour, IPointerClickHandler {

    public Image image;

    private static CanvasUI_ImageZoom _instance;
    public static CanvasUI_ImageZoom instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject imageZoomPrefab = Instantiate(Resources.Load("Prefabs/CanvasUI/CanvasUI_ImageZoom") as GameObject);
                _instance = imageZoomPrefab.GetComponent<CanvasUI_ImageZoom>();
            }
            return _instance;
        }
        protected set { }
    }
    
	public void zoomIn(Image newImage)
    {
        image.sprite = newImage.sprite;
        this.gameObject.SetActive(true);
    }

    public void zoomOut()
    {
        this.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        zoomOut();
    }
}
