﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CanvasUI_Notification : MonoBehaviour {

    public Text notificationText;
    public Button okButton;
    
    void Start()
    {
        okButton.onClick.AddListener(OnClose);
    }

    public void Setup(string notification)
    {
        notificationText.text = notification;
    }
    
    void OnClose()
    {
        GameManager.Instance.SetCurrentStateTo(GAME_STATE.IN_GAME);
        SoundManager.Instance.PlaySound(SOUND_CLIPS.OPENCLOSE, CanvasManager.Instance.transform);
        GameManager.Instance.SetTimeScale(1.0f);
    }

}
