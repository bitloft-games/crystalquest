﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUI_CrystalQuestion : MonoBehaviour
{

    public QuizUI quizUI;
    public GameObject questionPanel;
    public GameObject resultCorrect;
    public GameObject resultIncorrect;

    private QuizQuestion question;

    void Start()
    {
        question = new QuizQuestion();
        question.body = new QuizQuestion.Body("This is the crystal question");
        QuizQuestion.Interaction interaction =
            new QuizQuestion.Interaction(new List<QuizQuestion.Choice>() {
            new QuizQuestion.Choice("Choice 1 (pssst! pick this one)", "1"),
            new QuizQuestion.Choice("Choice 2", "2"),
            new QuizQuestion.Choice("Choice 3", "3"),
            new QuizQuestion.Choice("Choice 4", "4") });
        
        question.interaction = interaction;
        question.answer = new QuizQuestion.Answer("1");

        quizUI.DisplayQuestion(question);
        
    }

    public void onSubmit()
    {
        questionPanel.SetActive(false);

        if (question.playerAnsweredCorrectly())
        {
            resultCorrect.SetActive(true);
            GameManager.Instance.IncreaseEnergy(1);
        }
        else
        {
            resultIncorrect.SetActive(true);
        }
    }

    public void onClose()
    {
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.INVENTORY);
    }
}