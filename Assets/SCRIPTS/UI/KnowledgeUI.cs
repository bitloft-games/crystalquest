﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using RestSharp.Contrib;

[ExecuteInEditMode]
public class KnowledgeUI : DynamicUI
{

    public TextAsset KnowledgeTextAsset;
    [TextArea(5, 20)]
    public string editorText;
    
    public GameObject h1TextPrefab;
    public GameObject h2TextPrefab;
    public GameObject pTextPrefab;
    public GameObject liTextPrefab;
    public GameObject imgPrefab;

    private List<GameObject> UIObjectList;
    private string imagepath = "Assets/Resources/crystals/crystal_media/";
    private string imageURL = "https://storage.googleapis.com/biology-images/crystal_media/";

    protected override void CreateChildren()
    {
        string input;

        // If the text was entered manually in the editor's text field,
        // use that instead of whatever file that was linked.
        if (editorText.Length > 0) input = editorText;
        else input = KnowledgeTextAsset.text;

        UIObjectList = CreateChildrenRecursive(RemoveWhitespace(input));

        foreach (GameObject go in UIObjectList)
        {
            go.transform.SetParent(transform, false);
        }
        
    }
    
    private List<GameObject> CreateChildrenRecursive(string input)
    {
        List<GameObject> returnList = new List<GameObject>();

        string filteredText = input.Trim();
        
        //Find and handle tag matches
        string searchString = "<(?<tagType>([^bi]+?|li))>(?<content>(?:.|\\n)*?)</\\1>|<(?<tagType>img) src=\"(?<image>.*)\" />";
        Regex htmlTagRegex = new Regex(searchString, RegexOptions.Multiline);
        Match match = htmlTagRegex.Match(filteredText);

        //Duck out early if no tags found
        if (!match.Success) {
            //If there is remaining text, it needs to be in a p tag
            if (filteredText != "") returnList.Add(createTextChild(pTextPrefab, HttpUtility.HtmlDecode(filteredText)));
            return returnList;
        }

        string precontent = filteredText.Substring(0, match.Index);
        string postcontent = filteredText.Substring(match.Index + match.Length);

        if (precontent != "") returnList.AddRange(CreateChildrenRecursive(precontent));

        string tag = match.Groups["tagType"].Value;
        string content = match.Groups["content"].Value;
        string image = match.Groups["image"].Value;
            
        switch (tag)
        {
            case "br":
                returnList.Add(createTextChild(pTextPrefab, ""));
                break;
            case "p":
                returnList.AddRange(CreateChildrenRecursive(content));
                break;
            case "h1":
                returnList.Add(createTextChild(h1TextPrefab, HttpUtility.HtmlDecode(content)));
                break;
            case "h2":
                returnList.Add(createTextChild(h2TextPrefab, HttpUtility.HtmlDecode(content)));
                break;
            case "h4":
                returnList.Add(createTextChild(h2TextPrefab, HttpUtility.HtmlDecode(content)));
                break;
            case "li":
                returnList.Add(createTextChild(liTextPrefab, HttpUtility.HtmlDecode(content)));
                break;
            case "ul":
                returnList.AddRange(CreateChildrenRecursive(content));
                break;
            case "img":
                returnList.Add(createIMGChild(imgPrefab, GetImagePath(image)));
                break;
            default:
                Debug.Log("Couldn't parse tag: " + tag);
                break;
        }
        
        if (postcontent != "") returnList.AddRange(CreateChildrenRecursive(postcontent));
        return returnList;
    }

    private string GetImagePath(string url)
    {
        //Unity spefically wants to know the image resource path without the extension
        string filename = url.Replace(imageURL, "");
        return imagepath + filename;
    }

    private string RemoveWhitespace(string s)
    {
        string removeWhite = "(<\\/[^>]+>)([\\s]+)(<[^>]+>)";
        Regex noWhite = new Regex(removeWhite, RegexOptions.Multiline);
        return noWhite.Replace(s, "$1$3");
    }
}
