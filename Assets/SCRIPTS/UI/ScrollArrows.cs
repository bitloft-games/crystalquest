﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollArrows : MonoBehaviour {

    public GameObject upArrow;
    public GameObject downArrow;
    //runs every frame so it needs to be reeaaally low for smooth movement.
    public float scrollSpeed = 0.0015f;

    private ScrollRect scrollRect;


	void Start () {
        scrollRect = this.GetComponent<ScrollRect>();
	}
	
	void Update () {

        float scrollpos = scrollRect.verticalNormalizedPosition;
       
        // decide which arrows to show
        if (scrollpos < 0.001f && scrollpos != 0)
        {
            downArrow.SetActive(false);
            upArrow.SetActive(true);
        } else if (scrollpos > 0.001f)
        {
            downArrow.SetActive(true);
            if (scrollpos > 0.999f) upArrow.SetActive(false);
            else upArrow.SetActive(true);
        } 
        
	}

    public void scrollUp()
    {
        scrollRect.verticalNormalizedPosition += scrollSpeed;
    }

    public void scrollDown()
    {
        scrollRect.verticalNormalizedPosition -= scrollSpeed;
    }

}
