﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class CanvasManager : SingletonManager<CanvasManager>
{
    public CANVAS_SCREEN currentScreen;
    public CANVAS_SCREEN previousScreen;
    public GameObject currentScreenObject;
    public GameObject GoBattleButton;
    public GameObject Button_Instructions;

    private void Start()
    {
        Messenger.AddListener("CloseCurrentScreen", CloseCurrentScreen);
		Messenger.AddListener<Goal> (GAME_EVENTS.GOAL_ACHIEVED.ToString (), OnGoalAchievedEvent);
    }

    void CloseCurrentScreen()
    {
        if (currentScreenObject != null)
        {
            Destroy(currentScreenObject);
        }
    }

    public void SetCurrentScreenTo(CANVAS_SCREEN screen)//Loads the UI screen
    {
        previousScreen = currentScreen;
        currentScreen = screen;

        Debug.Log("Canvas Screen :: currentScreen = " + currentScreen + " previousScreen = " + previousScreen);

        CloseCurrentScreen();

        switch (currentScreen)
        {
            case CANVAS_SCREEN.PLAYER_HUD:
                
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_InGame"));
                //CameraController.SharedInstance.SetCameraToDefaultMode();
                break;

            case CANVAS_SCREEN.MAP:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Map"));
                break;

            case CANVAS_SCREEN.DEBUG:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Debug"));
                break;

            case CANVAS_SCREEN.QUIZ:
                CameraManager.Instance.SetCameraMode(CameraManager.Mode.UI);
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Quiz"));
                break;
            case CANVAS_SCREEN.CRYSTAL_KNOWLEDGE:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_CrystalKnowledge1"));
                SoundManager.Instance.PlaySound(SOUND_CLIPS.UI, transform);
                CameraManager.Instance.SetCameraMode(CameraManager.Mode.UI);
                break;

            case CANVAS_SCREEN.INVENTORY:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Inventory"));
                break;

            case CANVAS_SCREEN.CRYSTAL_QUESTION:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_CrystalQuestion"));
                break;

            case CANVAS_SCREEN.LOADING:
                break;

			case CANVAS_SCREEN.CHARACTER_CUSTOMIZATION:
				currentScreenObject = Instantiate (ResourceManager.LoadCanvasObject ("CanvasUI_CharacterCustomization")); 
				break;
			case CANVAS_SCREEN.GOALS_SCREEN:
				currentScreenObject = Instantiate (ResourceManager.LoadCanvasObject ("CanvasUI_GoalsList")); 
				break;
            case CANVAS_SCREEN.YESNO:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_YESNO"));
                break;
            case CANVAS_SCREEN.NOTIFICATION:
                currentScreenObject = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Notification"));
                break;

        }

        if (!currentScreenObject.activeSelf)
        {
            currentScreenObject.SetActive(true);
        }

    }

    internal void SetCurrentScreenTo(object cRYSTAL_QUESTION)
    {
        throw new NotImplementedException();
    }

    public GameObject LoadingSpinner;

    public void ShowLoadingSpinner()
    {
        HideLoadingSpinner();
        if (LoadingSpinner == null)
        {
            LoadingSpinner = Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_Loading_Spinner"));
        }
    }


    public void HideLoadingSpinner()
    {
        if (LoadingSpinner != null)
        {
            Destroy(LoadingSpinner);
            LoadingSpinner = null;
        }
    }

    public void HideLoadings()
    {
        HideLoadingSpinner();
    }

	void OnGoalAchievedEvent(Goal goal){	
		
		GameObject obj	= Instantiate(ResourceManager.LoadCanvasObject("CanvasUI_GoalNotification"));

		obj.GetComponent<CanvasUI_GoalNotification> ().SetDetails (goal);
	}
}
