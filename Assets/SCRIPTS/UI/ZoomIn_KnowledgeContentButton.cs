﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomIn_KnowledgeContentButton : MonoBehaviour
{

    public Animator _KnowledgeAnimator;

    int tapCount;
    float tapOffset = 0.4f;
    float tapStartTime;
    int count;
    int index = 0;

    public void OnTap()
    {
        if (tapCount <= 0)
        {
            CancelInvoke("CheckForTap");
            InvokeRepeating("CheckForTap", 0.0f, 0.1f);
        }

        tapCount++;
        if (tapCount >= 2)
        {
            if (index == 0)
            {
                ZoomIn();
                index = 1;
            }
            else if (index == 1)
            {
                ZoomOut();
                index = 0;
            }
        }
        tapStartTime = Time.time;
    }

    void CheckForTap()
    {
        if (Time.time - tapStartTime >= tapOffset)
        {
            tapCount = 0;
            count = 0;
            CancelInvoke("CheckForTap");
        }
    }

    void ZoomIn()
    {
        _KnowledgeAnimator.Play("KnowledgeContentZoomIn");
    }
    void ZoomOut()
    {
        _KnowledgeAnimator.Play("KnowledgeContentZoomOut");
    }
}
