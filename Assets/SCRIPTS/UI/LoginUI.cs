﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoginUI : MonoBehaviour
{
    public InputField usernameField;
    public InputField passwordField;
    public Text response;
    public Canvas loginCanvas;
    public Toggle debugTestUserToggle;
    // Use this for initialization
    void Start()
    {
        OnTestUserToggleChanged();
    }

    public void OnTestUserToggleChanged()
    {
        if (debugTestUserToggle.isOn)
        {
            usernameField.text = "invga/test";
            passwordField.text = "test";
        }
        else
        {
            usernameField.text = "";
            passwordField.text = "";
        }
    }

    public void OnLogin()
    {
        LMSTool.Instance.AuthenticatePlayer(usernameField.text, passwordField.text).Then(user =>
        {
            response.text = "Welcome, " + user.firstname + "!";
            GameManager.Instance.LoadSceneAndSetCurrentStateTO("ProtoLobby", GAME_STATE.IN_GAME, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
        ).Fail(() => {
            response.text = "Failed!";
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
