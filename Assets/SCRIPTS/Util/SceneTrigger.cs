﻿using UnityEngine;
using System.Collections;

public class SceneTrigger : MonoBehaviour
{
    public string SceneName;
    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider other)
    {
     
        GameManager.Instance.LoadSceneAndSetCurrentStateTO(SceneName, GAME_STATE.IN_GAME, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
      
    // Update is called once per frame
    void Update()
    {

    }
}
