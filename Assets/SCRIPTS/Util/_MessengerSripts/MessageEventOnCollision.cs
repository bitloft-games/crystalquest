﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageEventOnCollision : MonoBehaviour
{
    public string MsgEventName;
    public bool isOneTimeEvent;
    bool isEventTrigger;
    void OnEnable()
    {
        isEventTrigger = false;
    }

    void OnTriggerEnter(Collider other)
    {

        if (isOneTimeEvent)
        {
            gameObject.SetActive(false);
            if (!isEventTrigger)
            {
                isEventTrigger = true;
                Messenger.Broadcast(MsgEventName);
            }
        }
        else
        {
            Messenger.Broadcast(MsgEventName);
        }

    }

    void OnCollisionEnter(Collision other)
    {
        if (isOneTimeEvent)
        {
            gameObject.SetActive(false);
            if (!isEventTrigger)
            {
                isEventTrigger = true;
                Messenger.Broadcast(MsgEventName);
            }
        }
        else
        {
            Messenger.Broadcast(MsgEventName);
        }
    }
}
