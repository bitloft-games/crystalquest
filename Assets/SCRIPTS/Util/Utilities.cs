﻿
using System.Collections;
using System.Collections.Generic;

//using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;


public static class Utilities
{
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < min)
            angle += 360;
        if (angle > max)
            angle -= 360;
        //		return angle;
        return Mathf.Clamp(angle, min, max);
    }

    public static Vector3 GetRandomPointOnNavMesh(Vector3 center, float range)
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(center, out hit, range, NavMesh.AllAreas)) // last paramater 0 means walkable area
        {

            return hit.position;
        }

        return Vector3.zero;

    }

}

