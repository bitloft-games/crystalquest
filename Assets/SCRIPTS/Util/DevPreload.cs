﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DevPreload : MonoBehaviour {
    void Awake()
    {
#if UNITY_EDITOR
        GameObject check = GameObject.Find("__app");
        if (check == null) {
            
            SceneManager.LoadScene("_preload", LoadSceneMode.Additive);
           
        }
#endif
    }

    private void Start()
    {
        GameManager.Instance.SetCurrentStateTo(GAME_STATE.IN_GAME);
    }

   
}
