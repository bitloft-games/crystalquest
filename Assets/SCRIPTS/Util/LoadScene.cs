﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public string nextScene;
    private bool doOnce = false;
    private void LateUpdate()
    {
        //for the sole purpose of loading a scene after waking

        // if we have more than one scene then this was probably loaded additively from another scene in editor mode.
        if (!doOnce)
        {
            if (SceneManager.sceneCount == 1)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
            }
            doOnce = true;
        }

    }
}
