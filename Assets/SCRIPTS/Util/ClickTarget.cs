﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickTarget : MonoBehaviour {
    public String OnClickEvent;
    public float Range;
	// Use this for initialization
	void Start () {
		
	}
	
	public void OnClick()
    {
        Debug.Log("Sending: " + OnClickEvent);
        this.BroadcastMessage(OnClickEvent);
    }
}
