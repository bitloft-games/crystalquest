﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideTarget : MonoBehaviour {
    public String OnClickEvent;
    public float Range;
	// Use this for initialization
	void Start () {
		
	}
	
	public void OnTriggerEnter()
    {
        Debug.Log("Sending: " + OnClickEvent);
        this.BroadcastMessage(OnClickEvent);
    }
}
