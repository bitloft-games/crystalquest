﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonManager<T> : MonoBehaviour where T : Component
{

    private static T _instance = default(T);

    public static T Instance
    {
        get
        {
            // if the instance hasn't been assigned then search for it
            if (_instance == null)
            {

                _instance = GameObject.FindObjectOfType<T>();
            }

            return _instance;
        }
        set
        {

            _instance = value;

        }
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
      
    }

}
