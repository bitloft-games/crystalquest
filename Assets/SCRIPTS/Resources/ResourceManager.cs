﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class ResourceManager
{

    public static void RefreshMemory()
    {
        Resources.UnloadUnusedAssets();
    }

    public static GameObject LoadCanvasObject(string canvasObjectName)
    {
        return (Resources.Load("Prefabs/CanvasUI/" + canvasObjectName) as GameObject);
    }
    public static GameObject LoadKnowledgeObject(string canvasObjectName)
    {
        return (Resources.Load("Prefabs/CanvasUI/Knowledge/" + canvasObjectName) as GameObject);
    }

    public static GameObject LoadEffect(string effectName)
    {
        return (Resources.Load("Prefabs/Effects/" + effectName) as GameObject);
    }

    public static Material LoadMaterial(string materialName)
    {
        return (Resources.Load("Materials/" + materialName) as Material);
    }

    public static Texture2D LoadTexture2D(string textureName)
    {
        return (Resources.Load<Texture2D>("Texture2D/" + textureName));
    }
    
    public static Sprite LoadSprite(string spriteName)
    {
        return (Resources.Load<Sprite>("Sprites/" + spriteName));
    }
    
    public static AudioClip LoadSoundClip(string ClipName)
    {
        return (Resources.Load<AudioClip>("Sounds/" + ClipName));
    }

    public static Texture2D LoadTexture2DfromImage(string filepath)
    {
        Texture2D texture;
        byte[] fileData;

        if (File.Exists(filepath))
        {
            fileData = File.ReadAllBytes(filepath);
            texture = new Texture2D(2, 2);
            if (texture.LoadImage(fileData))
                return texture;
        }
        return null;
    }

    public static Sprite LoadSpritefromImage(string filepath, float pixelsPerUnit = 100.0f)
    {
        Sprite sprite;
        Texture2D spriteTexture = LoadTexture2DfromImage(filepath);
        sprite = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height), new Vector2(0,0), pixelsPerUnit);

        return sprite;
    }
}
