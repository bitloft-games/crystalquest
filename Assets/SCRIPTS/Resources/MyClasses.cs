﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class CrystalData
{
    public Crystal[] crystalData;
}
[System.Serializable]
public class Crystal
{
    public string id;
    public string title;
    public bool isFound;
    public CrystalQuestion[] questions;
}
[System.Serializable]
public class CrystalQuestion
{
    public string question;
    public string answer;
    public CrystalAnswerOptions[] options;
}
[System.Serializable]
public class CrystalAnswerOptions
{
    public string option;
}