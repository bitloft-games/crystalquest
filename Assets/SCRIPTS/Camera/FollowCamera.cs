﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;


public class FollowCamera : CameraType {

    //If we want to zoom in out
    protected float zDistanceOffset = 0;
    protected Vector3 initialRotation;
    
    public Transform target;

    public Vector3 CameraDistance = new Vector3(0f, 3f, -6f);
    
    public float MouseSensitivityX = 10f;
    public float MouseSensitivityY = 5f;
    public float ScrollSensitivity = 2f;
    public float CameraSpeed = 2f;

    public float yMinLimit = 60;
    public float yMaxLimit = 140;
    
    // Use this for initialization
    public override void Start () {

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            ConnectTarget();
        }

        base.Start();
        MoveToDestination();
        LookAt(target.transform);
        initialRotation = transform.rotation.eulerAngles;
    }

    private void OnLevelWasLoaded(int level)
    {
        ConnectTarget();
    }

    void ConnectTarget()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void LateUpdate () {
		if (LeanTouch.PointOverGui (Input.mousePosition))
		{
			return;
		}
	    if (Input.GetMouseButton(0) && CameraManager.Instance.currentMode == CameraManager.Mode.Looking)
            LookAround();
        else if (CameraManager.Instance.currentMode == CameraManager.Mode.Follow)
            MoveToDestination();
        LookAt(target.transform);
    }

    private void MoveToDestination()
    {
        Vector3 destination;
        destination = target.transform.rotation * CameraDistance;
        destination += target.position;
        //Update position based on user movement and distance the camera should be
        transform.position = Vector3.Slerp(transform.position, destination, Time.deltaTime * CameraSpeed);
    }

    private void LookAround()
    {
        float x = Input.GetAxis("Mouse X") * MouseSensitivityX;
        initialRotation.x += x;
        transform.RotateAround(target.position, Vector3.up, x);

        /*
        float y = -Input.GetAxis("Mouse Y") * MouseSensitivityY;
        // should add something here to limit the y rotation
        // so we don't go through the floor
        transform.RotateAround(target.position, Vector3.right, y);
        initialRotation.y += y;*/
    }

    private void LookAt(Transform t)
    {
        //Look at the target (usually player)
        transform.LookAt(t);
    }
    
}
