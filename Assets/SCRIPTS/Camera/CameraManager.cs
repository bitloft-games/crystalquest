﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : SingletonManager<CameraManager>
{

    public CameraType followCamera;
    public CameraType UICamera;
    public CameraType battleCamera;

    public Mode defaultMode;
    public Mode currentMode;

    protected CameraType currentCameraType;
    
    public enum Mode
    {
        Follow,
        Battle,
        TargetFocus,
        Looking,
        UI
    }

    // Use this for initialization
    void Start () {
        DisableAllCameras();
        // This should be set from whatever game state controller we make,
        // Not set here by default.
        SetCameraMode(defaultMode);
    }
	
    public void SetCameraMode(Mode mode)
    {
        if (currentCameraType != null)
            currentCameraType.gameObject.SetActive(false);
        switch (mode) {
            case Mode.Battle:
                currentCameraType = battleCamera;
                break;
            case Mode.UI:
                currentCameraType = UICamera;
                break; 
            case Mode.Follow:
                currentCameraType = followCamera;
                break;
            default:
                currentCameraType = UICamera;
                break;
        }
        currentCameraType.gameObject.SetActive(true);
        currentMode = mode;
    }

    public Ray CameraRay(Vector3 mousePosition)
    {
        return currentCameraType.CameraRay(mousePosition);
    }

    public void DisableAllCameras()
    {
        followCamera.gameObject.SetActive(false);
        UICamera.gameObject.SetActive(false);
        battleCamera.gameObject.SetActive(false);
    }
}
