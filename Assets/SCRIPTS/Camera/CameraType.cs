﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CameraType : MonoBehaviour {
    protected Camera _camera;

    public virtual void Start ()
    {
        _camera = this.GetComponent<Camera>();
    }

    public Ray CameraRay(Vector3 mousePosition)
    {
        return _camera.ScreenPointToRay(mousePosition);
    }
}
