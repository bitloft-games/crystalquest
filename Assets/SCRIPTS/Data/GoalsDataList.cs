﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]  
public class Goal
{
	public GOAL_ID GoalID;
	public GAME_EVENTS ConditionToActivate;
	public GAME_EVENTS ConditionToComplete;
	public GAME_EVENTS CompleteEvent;
	public string Description;
	public string Message;
	public int Points;
	public bool IsActive;
	public bool IsAchieved;
}

[CreateAssetMenuAttribute]
public class GoalsDataList : ScriptableObject
{
	public List<Goal> GoalsData;
}