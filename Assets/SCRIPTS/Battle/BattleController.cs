﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleController : SingletonManager<BattleController> {
    public Animator PlayerAnim;
    public Enemy Enemy;
    public string BattleSceneName;

	// Use this for initialization
	void Start () {
        IsPlayerTurnToAttack = true;
	}
    public bool IsPlayerTurnToAttack
    {
        get;set;
    }
    public Enemy GetCurrentEnemy()
    {
        return Enemy;
    }
    bool isActive = false;
    private void OnLevelWasLoaded(int level)
    {
        if (SceneManager.GetActiveScene().name == BattleSceneName)
        {
            isActive = true;
            PlayerAnim = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
            Enemy = GameObject.FindGameObjectWithTag("AttackTarget").GetComponent<Enemy>();

        }
    }

    private void Update()
    {
        
        if (isActive && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "AttackTarget")
                {
                    if (GameManager.Instance.GetEnergy() > 0)
                    {
                        PlayerAnim.SetBool("isAttack", true);

                        GameManager.Instance.DecreaseEnergy(1);
                        
                    }
                    else
                    {
                        // make a dud sound and display you don't have enough energy
                    }
                }
            


            }
        }
    }
}
