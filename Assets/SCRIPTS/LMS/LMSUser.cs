﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LMSUser
{
    public string userid, username, firstname, lastname, email;
    public string domainid, domainname, userspace;
    public string token;
    public int authenticationexpirationminutes;

    //experimental stuff
    public string enrollmentid;

}