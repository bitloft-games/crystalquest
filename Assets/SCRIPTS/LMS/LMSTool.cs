﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using uPromise;

public class LMSTool : SingletonManager<LMSTool>
{
    public LMSUser player { get; private set; }
    public LMSUser instructor { get; private set; }
    public List<Course> courses { get; private set; }
    public List<Item> items { get; private set; }
    public List<QuizQuestion> questions { get; private set; }
    public List<Enrollment> enrollments { get; private set; }
    public Attempt attempt { get; private set; }

    private string testEnrollmentID;

    public string instructorUsername = "invga/Alex";
    public string instructorPassword = "teacher";

    protected Dictionary<string, string> jsonHeaders = new Dictionary<string, string>();

    //With this information prepopulated, we can pull from the LMS
    //public string targetDomainID;
    public string targetCourseID;
    public string targetQuizID;
    public string ResponseText
    {
        get; set;
    }
    protected string baseURL = "https://gls.agilix.com/cmd";

    void Start()
    {
        jsonHeaders["accept"] = "application/json";
        //jsonHeaders["Content-Type"] = "application/json";
    }

    //Authenticate player via LMS
    public Promise<LMSUser> AuthenticateSystemInstructor()
    {
        Deferred<LMSUser> def = new Deferred<LMSUser>();
        AuthenticateUser(instructorUsername, instructorPassword).Then(user =>
        {
            instructor = user;
            def.Resolve(user);
        });
        return def.Promise;
    }

    public Promise<LMSUser> AuthenticatePlayer(string userName, string password)
    {
        Deferred<LMSUser> defferedUser = new Deferred<LMSUser>();
        AuthenticateSystemInstructor().Then(() => AuthenticateUser(userName, password).Then(user =>
        {
            player = user;
            GetLoginInfo().Then(() => {
                defferedUser.Resolve(user);
            });
        }));
        
        return defferedUser.Promise;
    }

    private Promise<LMSUser> AuthenticateUser(string username, string password)
    {
        Deferred<LMSUser> loginDeferred = new Deferred<LMSUser>();
        WWWForm form = new WWWForm();
        form.AddField("cmd", "login2");
        form.AddField("username", username);
        form.AddField("password", password);

        byte[] rawData = form.data;
        WWW www = new WWW(baseURL, rawData, jsonHeaders);
        StartCoroutine(LoginRequest(www, loginDeferred));
        return loginDeferred.Promise;
    }

    IEnumerator LoginRequest(WWW www, Deferred<LMSUser> loginDeferred)
    {
        yield return www;
        LoginRO ro = JsonUtility.FromJson<LoginRO>(www.text);

        //Authentication was successful
        if (ro.response.code == "OK")
        {
            loginDeferred.Resolve(ro.response.user);
        }
        else
        {
            Debug.Log("Problem Logging in. Error: " + ro.response.code);
        }
          
    }

    Deferred GetLoginInfo()
    {
       
        return GetUserEnrollments();

        //doesn't need enrollment id but doesn't supply user's previous answers
        //GetQuizQuestions(); 
    }
    
    /* Decided to do this differently but it does work. Keep this here in case we need to use it again.
        //Get Quiz questions from authenticated LMS session
        public void GetQuizQuestions()
        {
            //TODO change this command to get quiz questions once we figure out what we need
            string queryURL = GetAuthQuery("listquestions") + "&entityid=" + targetCourseID + "&itemid="+targetQuizID;
            WWW www = new WWW(queryURL, null, jsonHeaders);
            StartCoroutine(GetQuizQuestionsRequest(www));
        }

        IEnumerator GetQuizQuestionsRequest(WWW www)
        {
            yield return www;
            string cleaned = CleanJson(www.text);
            ListQuestionsRO ro = JsonUtility.FromJson<ListQuestionsRO>(cleaned);
            if (ro.response.code == "OK")
            {
                questions = ro.response.question;
                StartQuiz();
            }
            else
            {
                Debug.Log("Problem getting quiz questions. Error: " + ro.response.code);
            }
        }
        */

    //Get Course list from authenticated LMS session
    public void GetCourseList()
    {
        string queryURL = GetAuthQuery("listcourses") + "&domainid=" + player.domainid + "&includedescendantdomains=false";
        WWW www = new WWW(queryURL, null, jsonHeaders);
        StartCoroutine(GetCourseListRequest(www));
    }

    IEnumerator GetCourseListRequest(WWW www)
    {
        yield return www;
        CourseListRO ro = JsonUtility.FromJson<CourseListRO>(www.text);
        if (ro.response.code == "OK")
        {
            courses = ro.response.courses.course;
        }
        else
            Debug.Log("Error in getting course list.");
    }

    public Deferred GetUserEnrollments()
    {
        Deferred enrollmentsDeferred = new Deferred();
        string query = GetAuthQuery("listuserenrollments") + "&userid=" + player.userid;
        WWW www = new WWW(query, null, jsonHeaders);
        StartCoroutine(GetUserEnrollmentRequest(www, enrollmentsDeferred));
        return enrollmentsDeferred;
    }

    IEnumerator GetUserEnrollmentRequest(WWW www, Deferred enrollmentsDeferred)
    {
        yield return www;
        UserEnrollmentRO ro = JsonUtility.FromJson<UserEnrollmentRO>(www.text);
        if (ro.response.code == "OK")
        {
            enrollments = ro.response.enrollments.enrollment;
            //for testing purposes, this shouldn't stay in
            testEnrollmentID = enrollments[0].id;
            GetAttempt().Then(() => enrollmentsDeferred.Resolve()).Fail(() => enrollmentsDeferred.Reject());
        }
        else
        {
            enrollmentsDeferred.Reject();
            Debug.Log("Error in getting player enrollments: " + ro.response.message);
        }
    }

    public Promise SaveAttemptAnswers()
    {
        Deferred saveAttemptDeferred = new Deferred();

        AttemptAnswerRequest request = new AttemptAnswerRequest();
        request.enrollmentid = testEnrollmentID;
        request.itemid = targetQuizID;
        request.page = attempt.page;
        List<Submission> submissions = new List<Submission>();

        foreach (QuizQuestion qq in questions)
        {
            Submission submission = new Submission(qq.partid, qq.getPlayerAnswer());
            submissions.Add(submission);
        }
        request.submission = submissions;

        AttemptAnswerRequestObject requestObject = new AttemptAnswerRequestObject(request);
        string requestJson = JsonUtility.ToJson(requestObject);
        requestJson = DirtyJson(requestJson);
        byte[] rawData = Encoding.ASCII.GetBytes(requestJson);
        jsonHeaders["Content-Type"] = "application/json";

        WWW www = new WWW(GetAuthQuery("submitattemptanswers"), rawData, jsonHeaders);
        StartCoroutine(SaveAttemptAnswerRequest(www, saveAttemptDeferred));

        return saveAttemptDeferred.Promise;
    }

    IEnumerator SaveAttemptAnswerRequest(WWW www, Deferred saveAttemptDeferred)
    {
        yield return www;
        SaveAttemptAnswerRO ro = JsonUtility.FromJson<SaveAttemptAnswerRO>(www.text);

        //Authentication was successful
        if (ro.response.code == "OK")
        {
            saveAttemptDeferred.Resolve();
            Debug.Log("Attempt saved.");
        }
        else
        {
            saveAttemptDeferred.Reject();
            Debug.Log("Problem Saving attempt. Error: " + ro.response.message);
        }
            
    }

    [Serializable]
    private class SaveAttemptAnswerRO
    {
        public SaveAttemptResponse response;
    }

    [Serializable]
    private class SaveAttemptResponse : Response
    {
        public Answers answers;
    }

    [Serializable]
    private class Answers
    {
        public QuizQuestion.Value version;
    }

    [Serializable]
    private class AttemptAnswerRequestObject //Request
    {
        public AttemptAnswerRequest request;

        public AttemptAnswerRequestObject(AttemptAnswerRequest request)
        {
            this.request = request;
        }
    }

    [Serializable]
    private class AttemptAnswerRequest
    {
        public string cmd;
        public string enrollmentid, itemid; //also groupid if needed
        public int page, seconds;
        public List<Submission> submission;
    }

    [Serializable]
    public class Submission
    {
        public string partid;
        public QuizQuestion.Value answer;
        //public QuizQuestion.Answer notes;

        public Submission(string partid, string answerVal)
        {
            this.partid = partid;
            answer = new QuizQuestion.Value(answerVal);
        }

        [Serializable]
        public class Answer
        {
            public QuizQuestion.Value value;
        }
    }

    public Deferred GetAttempt()
    {
        Deferred attemptDeferred = new Deferred();
        string query = GetAuthQuery("getattempt") + "&enrollmentid=" + testEnrollmentID + "&itemid=" + targetQuizID;
        WWW www = new WWW(query, null, jsonHeaders);
        StartCoroutine(GetAttemptRequest(www, attemptDeferred));
        return attemptDeferred;
    }

    IEnumerator GetAttemptRequest(WWW www, Deferred attemptDeferred)
    {
        yield return www;
        GetAttemptRO ro = JsonUtility.FromJson<GetAttemptRO>(CleanJson(www.text));
        if (ro.response.code == "OK")
        {
            questions = ro.response.attempt.question;
            attempt = ro.response.attempt;

            // this is the end of the login chain
            attemptDeferred.Resolve();
        }
        else
        {
            attemptDeferred.Reject();
            Debug.Log("Problem getting quiz questions. Error: " + ro.response.code);
        }
    }

    [Serializable]
    public class GetAttemptRO
    {
        public GetAttemptResponse response;

        [Serializable]
        public class GetAttemptResponse : Response
        {
            public Attempt attempt;
        }
    }

    [Serializable]
    public class Attempt
    {
        public int page;
        public bool save;
        public int seconds;
        public bool adaptive;
        public List<QuizQuestion> question;
    }

    public Promise<Grade> GetGrade(/*string enrollmentID, string itemid*/)
    {
        Deferred<Grade> getGradeDeferred = new Deferred<Grade>();
        string queryURL = GetAuthQuery("getgrade") + "&enrollmentid=" + testEnrollmentID + "&itemid=" + targetQuizID;
        WWW www = new WWW(queryURL, null, jsonHeaders);
        StartCoroutine(GetGradeRequest(www, getGradeDeferred));
        return getGradeDeferred.Promise;
    }

    IEnumerator GetGradeRequest(WWW www, Deferred getGradeDeferred)
    {
        yield return www;

        GetGradeRO ro = JsonUtility.FromJson<GetGradeRO>(CleanJson(www.text));
        if (ro.response.code == "OK")
        {
            getGradeDeferred.Resolve(ro.response.grade);    
        }
        else
        {
            getGradeDeferred.Reject();
            Debug.Log("Problem getting quiz grade. Error: " + ro.response.code);
        }
    }

    [Serializable]
    public class GetGradeRO
    {
        public GetGradeResponse response;

        [Serializable]
        public class GetGradeResponse : Response
        {
            public Grade grade;
        }
    }

    [Serializable]
    public class Grade
    {
        public GradeStatus status;
        public int scoredversion, responseversion, attempts, seconds;
        public DateTime scoreddate, submitteddate, firstactivitydate, lastactivitydate;
        public double achieved, possible, rawachieved, rawpossible;
        public string letter;
    }

    [Flags]
    public enum GradeStatus
    {
        None = 0x00,
        Completed = 0x01,
        ShowScore = 0x04,
        AllowResubmission = 0x08,
        IgnoreDueDate = 0x10,
        SubmittedByTeacher = 0x20,
        Started = 0x40,
        Excluded = 0x80,
        Released = 0x100,
        ExtraCredit = 0x200,
        NeedsGrading = 0x400,
        PasswordVerified = 0x800,
        SkipMasteryRestriction = 0x1000,
        PostDueDateZero = 0x2000,
        ScoredByTeacher = 0x4000,
        HasNotes = 0x8000,
    };

    //Get Item list from authenticated LMS session
    public void GetItemList()
    {
        //In this example, entity id is the class number
        string queryURL = GetAuthQuery("getitemlist") + "&entityid=" + targetCourseID;
        WWW www = new WWW(queryURL, null, jsonHeaders);
        StartCoroutine(GetItemListRequest(www));
    }

    IEnumerator GetItemListRequest(WWW www)
    {
        yield return www;
        ItemListRO ro = JsonUtility.FromJson<ItemListRO>(www.text);
        if (ro.response.code == "OK")
        {
            items = ro.response.items.item;
        }
    }

    string GetAuthQuery(string command)
    {
        return baseURL + "?cmd=" + command + "&_token=" + instructor.token;
    }

    string CleanJson(string jsonString)
    {
        return jsonString.Replace("$value", "value").Replace("&#160;", " ");
    }

    string DirtyJson(string jsonString)
    {
        return jsonString.Replace("\"value\"", "\"$value\"");
    }

    IEnumerator JsonRequest(WWW www)
    {
        yield return www;
        ResponseText = www.text;
    }

    //Definitions for serializable json the lms will send back
    [Serializable]
    public class Response
    {
        public string code, message, errorId;
    }

    [Serializable]
    public class LoginRO
    {
        public LoginResponse response;

        [Serializable]
        public class LoginResponse : Response
        {
            public LMSUser user;
        }
    }

    [Serializable]
    public class UserEnrollmentRO
    {
        public UserEnrollmentResponse response;

        [Serializable]
        public class UserEnrollmentResponse : Response
        {
            public Enrollments enrollments;
        }

        [Serializable]
        public class Enrollments
        {
            public List<Enrollment> enrollment;
        }
    }

    [Serializable]
    public class Enrollment
    {
        public string id, courseid, guid;
    }

    //Objects Required for List Questions Command
    [Serializable]
    public class ListQuestionsRO
    {
        public ListQuestionsResponse response;

        [Serializable]
        public class ListQuestionsResponse : Response
        {
            public List<QuizQuestion> question;
        }
    }

    [Serializable]
    private class CourseListRO
    {
        public CourseListResponse response;

        [Serializable]
        public class CourseListResponse : Response
        {
            public Courses courses;
        }
    }

    [Serializable]
    public class Courses
    {
        public List<Course> course;
    }

    [Serializable]
    public class Course
    {
        public string id, title;
    }

    [Serializable]
    private class ItemListRO
    {
        public ItemListResponse response;

        [Serializable]
        public class ItemListResponse : Response
        {
            public Items items;
        }
    }

    [Serializable]
    public class Items
    {
        public List<Item> item;
    }

    [Serializable]
    public class Item
    {
        public ItemData data;
        public string id, version, resourceentityid, actualentityid;
    }

    [Serializable]
    public class ItemData
    {
        public ItemType type;
    }

    [Serializable]
    public class ItemType
    {
        public string value;
    }

}
