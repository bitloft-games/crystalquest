﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuizQuestion { 

    public string questionid;
    public Answer answer;
    public Body body;
    public Interaction interaction;

    //from getattempt
    public string partid, number;
    public int page;
    
    public string getPlayerAnswer()
    {
        foreach (Choice c in interaction.choice)
        {
            if ( c.selected)
            {
                return c.id;
            }
        }
        return "";
    }

    public Boolean playerAnsweredCorrectly()
    {
        // in the future, this should check all correct answers against all chosen
        // but for now, since it's just multiple choice and only one answer,
        // we can get away with this.
        return getPlayerAnswer() == answer.value[0].value;
    }

    public string prettyPrint()
    {
        string toprint = "";

        try
        {
            toprint += questionid + "\n";
            toprint += body.value + "\n";
            foreach (Choice choice in interaction.choice)
            {
                toprint += "\t" + choice.id + ". " + choice.body.value + "\n";
            }
            foreach (Value v in answer.value)
            {
                toprint += "\tanswer: " + v.value + "\n";
            }
        }
        catch
        {
            Debug.Log("Problem with question " + questionid);
            return "";
        }

        return toprint;
    }

    [Serializable]
    public class Choice
    {
        public string id, pscorem;
        public Body body;
        public bool selected;

        public Choice(string text, string id)
        {
            selected = false;
            body = new Body(text);
            this.id = id;
        }
    }

    [Serializable]
    public struct Answer
    {
        public List<Value> value;
        public Answer(string answerid)
        {
            this.value = new List<Value>() {
                new Value(answerid)
            };
        }
    }

    [Serializable]
    public class Value
    {
        public string value;

        public Value (string value)
        {
            this.value = value;
        }
    }

    [Flags]
    public enum InteractionFlags
    {
        None = 0x0,
        ShowWorkspace = 0x1,
        MaintainOrder = 0x2,
        Inline = 0x4,
        Multiple = 0x8,
        ExtraCredit = 0x200,
        ShowCorrectAnswers = 0x400,
        SidePassage = 0x800,
        PassageSingleQuestion = 0x1000,
        UnsafeHtml = 0x2000,
    };

    [Serializable]
    public class Interaction
    {
        public string type;
        public InteractionFlags flags;
        public List<Choice> choice;
        public int count;
        public string data;
        public int height, width;
        public string texttype;
        public string label;

        public Interaction(List<Choice> choices)
        {
            choice = choices;
        }
    }
    
    [Serializable]
    public struct Body
    {
        public string value;
        public Body(string value)
        {
            this.value = value;
        }
    }

}
