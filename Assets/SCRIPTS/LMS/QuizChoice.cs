﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizChoice : MonoBehaviour {

    private QuizQuestion.Choice _choice;
    private Toggle toggle;

    void Start()
    {
        toggle = gameObject.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(OnToggle);
        if (_choice.selected)
        {
            toggle.isOn = true;
        }
    }

    public void SetChoice(QuizQuestion.Choice choice)
    {
        _choice = choice;
    }
    
    public void OnToggle(bool isclick)
    {
        _choice.selected = toggle.isOn;
    }
}
