﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalCard_Script : MonoBehaviour
{
    public int cardIndex;
    public Text Crystal_Topic;
    public Image Crystal_Image;
    public Image NotFound;
    public Sprite Blank_Level;
    public Crystal crystal;
    
    void Start()
    {
    }
    
    public void OnCardInspect()
    {
        Messenger.Broadcast<Crystal>(GAME_COMMANDS.SHOW_CRYSTAL.ToString(), crystal);
    }

    public void OnCardMine()
    {
        Messenger.Broadcast<Crystal>(GAME_COMMANDS.MINE_CRYSTAL.ToString(), crystal);
    }

    public void SetCardDetails(Crystal crystal)
    {
        this.crystal = crystal;
        if (crystal.isFound)
        {
            Crystal_Topic.text = crystal.title;
        }
        else // here we will want to deactivate the card and activate a UI item of a blank crystal card
        {
            Crystal_Topic.text = "NF";
        }

    }

}
