﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crystal_Interaction : MonoBehaviour
{
    public bool isPlayerInArea;
    public bool isObjectCollected;
    //public LayerMask ignoreLayers;
    //public Transform crystal;
    public string crystalId;



    void Start()
    {
    }

    void OnCrystalClicked()
    {
        SoundManager.Instance.PlaySound(SOUND_CLIPS.UI, transform);//Crystal Clicked sound
        Messenger.Broadcast<string>(GAME_EVENTS.CRYSTAL_FOUND.ToString(), this.crystalId);

        this.gameObject.SetActive(false);
    }



}
