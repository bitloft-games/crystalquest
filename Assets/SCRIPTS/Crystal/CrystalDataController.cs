﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalDataController : SingletonManager<CrystalDataController>
{
    public TextAsset cardJsonData;
    public Dictionary<string,Crystal> CrystalDictionary;
    private Crystal[] _listOfIds;
    public Dictionary<string, Crystal> LoadCrystalData()
    {
       return  LoadCrystalDataFromJson(cardJsonData.text);

    }

    public Dictionary<string, Crystal> LoadCrystalDataFromJson(string cardJsonData)//Get the data from Json
    {
        Dictionary<string, Crystal> crystalList = new Dictionary<string,Crystal>();
        _listOfIds = JsonUtility.FromJson<CrystalData>(cardJsonData).crystalData;
        foreach(Crystal crystal in _listOfIds)
        {
            crystalList.Add(crystal.id, GetCrystalJSON(crystal.id));
        }
        return crystalList;
    }

    public Crystal GetCrystalJSON(string crystalId)//get a Particular CrystalCard ID data 
    {
        TextAsset crystalJSON = Resources.Load("crystals/" + crystalId + "_json") as TextAsset;
        Crystal data = JsonUtility.FromJson<Crystal>(crystalJSON.text);
        return data;
    }

}
