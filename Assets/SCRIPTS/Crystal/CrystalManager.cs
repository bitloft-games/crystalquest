﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalManager : SingletonManager<CrystalManager>
{
    public ArrayList crystalSortList = new ArrayList();
    public Dictionary<string, Crystal> CrystalDictionary;
    public Crystal currentCrystalCard;
    public int crystalFoundCounter;
    private void Awake()
    {
        Messenger.AddListener<string>(GAME_EVENTS.CRYSTAL_FOUND.ToString(),OnCrystalFound);
        Messenger.AddListener<Crystal>(GAME_COMMANDS.SHOW_CRYSTAL.ToString(), crystal => ShowCrystal(crystal));
        Messenger.AddListener<Crystal>(GAME_COMMANDS.MINE_CRYSTAL.ToString(), crystal => MineCrystal(crystal));
    }

    public int CrystalsFound()
    {
        return crystalFoundCounter;
    }

    void ShowCrystal(Crystal crystal)
    {
        currentCrystalCard = crystal;
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.CRYSTAL_KNOWLEDGE);

    }

    void MineCrystal(Crystal crystal)
    {
        currentCrystalCard = crystal;
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.CRYSTAL_QUESTION);
    }

    // crystal json data is loaded now
    void Start()
    {
        CrystalDictionary = CrystalDataController.Instance.LoadCrystalData();
    }

    void OnCrystalFound(string crystalId)
    {
        crystalFoundCounter++;
        AddCrystalIdToCollectdList(crystalId);
        //Crystal is not Locked
        currentCrystalCard = CrystalDictionary[crystalId];//Assigns the CrystalId to Open Knowledge Relevant to Id
        currentCrystalCard.isFound = true;
        UserAnalytics.Instance.CrystalCollected(crystalId);
    }

    // for playtest and debugging
    public void EnableAllCrystals()
    {
        foreach(Crystal card in CrystalDictionary.Values)
        {
            card.isFound = true;
            
        }
        crystalFoundCounter = CrystalDictionary.Count;
    }


    public void DisableAllCrystals()
    {
        foreach (Crystal card in CrystalDictionary.Values)
        {
            card.isFound = false;
        }
    }
    
    public int TotalCrystalsCollected //returns the value -number of crystals collected
    {
        get
        {
            return CrystalDictionary.Count;
        }
    }
    
    public void AddCrystalIdToCollectdList(string crystalId)//Adds crystal to the list when collected
    {
        crystalSortList.Add(crystalId);
        GameManager.Instance.playerProfile.UpdatePlayerXP();//Increases the Player XP Level After collecting the crystal
    }
    
    public Dictionary<string, ArrayList> questionIndexData = new Dictionary<string, ArrayList>();

    public int GetQuestionIndex(string crystalId)
    {
        //set the question Index 
        Crystal crystalCard = CrystalDictionary[crystalId];

        if (!questionIndexData.ContainsKey(crystalId))
        {
            ArrayList questionList = new ArrayList();
            //Add's the question List to the crystals acc. to the available questions in crystalcardjson
            for (int i = 0; i < crystalCard.questions.Length; i++)
            {
                questionList.Add(i);
            }
            questionIndexData.Add(crystalId, questionList);

        }

        if (questionIndexData[crystalId].Count <= 0)
        {

            for (int i = 0; i < crystalCard.questions.Length; i++)
            {
                questionIndexData[crystalId].Add(i);
            }

        }
        //Random index is taken to display question among the question list
        int index = (int)questionIndexData[crystalId][Random.Range(0, questionIndexData[crystalId].Count)];
        questionIndexData[crystalId].Remove(index);//asked index is removed from the list

        return index;

    }

}

