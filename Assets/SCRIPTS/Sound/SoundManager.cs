﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SoundManager : SingletonManager<SoundManager>
{


    public void InitOnFirstLaunch()
    {
        if (!PlayerPrefs.HasKey("SoundMode"))
        {
            SetSoundMode(true);
        }
        if (!PlayerPrefs.HasKey("MusicMode"))
        {
            SetMusicMode(true);
        }
    }


    public bool GetSoundMode()
    {
        return (PlayerPrefs.GetInt("SoundMode") == 1 ? true : false);
    }

    public void SetSoundMode(bool soundMode)
    {
        PlayerPrefs.SetInt("SoundMode", soundMode == true ? 1 : 0);
    }

    public bool GetMusicMode()
    {
        return (PlayerPrefs.GetInt("MusicMode") == 1 ? true : false);
    }

    public void SetMusicMode(bool musicMode)
    {
        PlayerPrefs.SetInt("MusicMode", musicMode == true ? 1 : 0);
        foreach (AudioSource audioSource in ActiveMusicObject)
        {
            if (audioSource != null)
            {
                audioSource.mute = !GetMusicMode();
            }
        }
    }

    public void PlaySound(SOUND_CLIPS soundClip, Transform sourceObject, bool playOnAwake = false)
    {
        //			if (GetSoundMode () == false) {
        //				return;
        //			}
        AudioSource audioSource = sourceObject.GetComponent<AudioSource>();
        if (audioSource == null)
        {
            sourceObject.gameObject.AddComponent<AudioSource>();
            audioSource = sourceObject.GetComponent<AudioSource>();
        }
        //if (audioSource.clip == null) {
        audioSource.clip = ResourceManager.LoadSoundClip("" + soundClip);
        //}
        audioSource.loop = false;
        audioSource.playOnAwake = playOnAwake;
        audioSource.Play();
    }

    List<AudioSource> ActiveMusicObject = new List<AudioSource>();

    public void PlayMusic(SOUND_CLIPS soundClip, Transform sourceObject, float volume)
    {

        AudioSource audioSource = sourceObject.GetComponent<AudioSource>();
        if (audioSource == null)
        {
            sourceObject.gameObject.AddComponent<AudioSource>();
            audioSource = sourceObject.GetComponent<AudioSource>();
        }
        //if (audioSource.clip == null) {
        audioSource.clip = ResourceManager.LoadSoundClip("" + soundClip);
        //}
        audioSource.loop = true;
        audioSource.Play();
        audioSource.volume = volume;
        audioSource.mute = !GetMusicMode();


        if (!ActiveMusicObject.Contains(audioSource))
        {
            ActiveMusicObject.Add(audioSource);
        }
    }

    public void StopMusic(SOUND_CLIPS soundClip, Transform sourceObject)
    {
        AudioSource audioSource = sourceObject.GetComponent<AudioSource>();
        if (audioSource == null)
        {
            return;
        }
        audioSource.Stop();
    }
    public void MuteMusic(SOUND_CLIPS soundClip, Transform sourceObject, bool muteFlag = true)
    {
        AudioSource audioSource = sourceObject.GetComponent<AudioSource>();
        if (audioSource == null)
        {
            return;
        }
        audioSource.mute = muteFlag;
    }
}