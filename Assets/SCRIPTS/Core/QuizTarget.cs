﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizTarget : MonoBehaviour {

	void OnTargetClicked ()
    {
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.QUIZ);
    }
}
