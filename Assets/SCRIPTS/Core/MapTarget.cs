﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTarget : MonoBehaviour {

	void OnTargetClicked()
    {
        showMap();
    }
    
    private void showMap()
    {
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.MAP);
    }

}
