﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalsManager : SingletonManager<GoalsManager>
{

	public GoalsDataList goalsDataListScriptableObject;


	public Dictionary<GOAL_ID,Goal> GoalsData;

	public List<GOAL_ID> ActiveGoalsList;
	public List<GOAL_ID> InActiveGoalsList;
	public List<GOAL_ID> AchievedGoalsList;


	void OnEnable()
	{
		Messenger.OnBroadCastEvent += CheckForGoalConditions;
	}


	void OnDisable()
	{
		Messenger.OnBroadCastEvent -= CheckForGoalConditions;
	}
	// Use this for initialization
	void Start ()
	{
		
		
		GoalsData = new Dictionary<GOAL_ID,Goal> ();

		ActiveGoalsList = new List<GOAL_ID> ();
		InActiveGoalsList = new List<GOAL_ID> ();
		AchievedGoalsList = new List<GOAL_ID> ();

		//Messenger.AddListener<GAME_EVENTS> (GAME_EVENTS.CHECK_FOR_GOAL_CONDITON.ToString (), CheckForGoalConditions);

		foreach (var data in goalsDataListScriptableObject.GoalsData)
		{
			GoalsData.Add (data.GoalID, data);

	
			if (data.IsAchieved)
			{
				AddGoalTypeToAchievedList (data.GoalID);

			} else if (data.IsActive)
			{
				AddGoalTypeToActiveList (data.GoalID);

			} else
			{
				AddGoalTypeToInActiveList (data.GoalID);
			}

		}

		// TO test only
		Messenger.AddListener(GAME_EVENTS.LEVEL_LOAD.ToString (),LevelLoad);
		Messenger.AddListener(GAME_EVENTS.LEVEL_STARTED.ToString (),LevelStarted);

		//Messenger.Broadcast (GAME_EVENTS.LEVEL_LOAD.ToString ());
		//Invoke ("check",2.0f);
	}
	void  check(){

		Messenger.Broadcast (GAME_EVENTS.LEVEL_STARTED.ToString ());

	}
	void LevelLoad(){
		
	}
	void LevelStarted(){

	}

	public void CheckForGoalConditions (string gameEvent)
	{

		GOAL_ID goalId = GOAL_ID.NONE;

		foreach (var key in ActiveGoalsList)
		{
			if (GoalsData [key].ConditionToComplete.ToString() == gameEvent)
			{

				Debug.Log ("Goal Complete " + GoalsData [key].GoalID);
				Debug.Log ("Goal Message " + GoalsData [key].Message);

				goalId = key;			
				break;
			}
		}

		if(goalId!= GOAL_ID.NONE){
			
			AddGoalTypeToAchievedList (goalId);
			Messenger.Broadcast (GAME_EVENTS.GOAL_ACHIEVED.ToString (),GoalsManager.Instance.GoalsData [goalId]);

		}


		foreach (var key in InActiveGoalsList)
		{
			if (GoalsData [key].ConditionToActivate.ToString() == gameEvent)
			{

				Debug.Log ("Goal Activated  " + GoalsData [key].GoalID);
				Debug.Log ("Goal Message " + GoalsData [key].Message);
				goalId = key;
				break;

			}
		}

		if(goalId!= GOAL_ID.NONE){

			AddGoalTypeToActiveList (goalId);
		}

	}

	public Goal GetCurrentActiveGoal(){
		return GoalsData [ActiveGoalsList [ActiveGoalsList.Count - 1]];
	}



	void AddGoalTypeToActiveList (GOAL_ID goalId)
	{
		if (!ActiveGoalsList.Contains (goalId))
		{
			ActiveGoalsList.Add (goalId);
		}

		if (InActiveGoalsList.Contains (goalId))
		{
			InActiveGoalsList.Remove (goalId);
		}

		GoalsData [goalId].IsActive = true;
	}

	void AddGoalTypeToInActiveList (GOAL_ID goalId)
	{
		
		if (!InActiveGoalsList.Contains (goalId))
		{
			InActiveGoalsList.Add (goalId);
		}

		if (ActiveGoalsList.Contains (goalId))
		{
			ActiveGoalsList.Remove (goalId);
		}

		GoalsData [goalId].IsActive = false;
	}

	void AddGoalTypeToAchievedList (GOAL_ID goalId)
	{

		GoalsData [goalId].IsAchieved = true;


		if (!AchievedGoalsList.Contains (goalId))
		{
			AchievedGoalsList.Add (goalId);
		}

		if (InActiveGoalsList.Contains (goalId))
		{
			InActiveGoalsList.Remove (goalId);
		}

		if (ActiveGoalsList.Contains (goalId))
		{
			ActiveGoalsList.Remove (goalId);
		}

	}

}



