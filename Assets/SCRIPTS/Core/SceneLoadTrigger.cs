﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadTrigger : MonoBehaviour {

    public string newScene;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.LoadSceneAndSetCurrentStateTO(newScene, GAME_STATE.IN_GAME, LoadSceneMode.Single);
        }
    }
}
