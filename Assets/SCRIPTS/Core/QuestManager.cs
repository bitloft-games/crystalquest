﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : SingletonManager<QuestManager> {

    public Quest getCurrentQuest()
    {
        return new Quest() { Description = "Find a crystal." };
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class Quest
{
    public string Description { get; set; }
}
