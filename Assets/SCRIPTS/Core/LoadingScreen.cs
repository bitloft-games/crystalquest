﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public void LoadIngameScene()
    { 
        GameManager.Instance.LoadSceneAndSetCurrentStateTO("InGame", GAME_STATE.IN_GAME, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public void Update()
    {


    }
}
