﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UMA.CharacterSystem;


public class GameManager : SingletonManager<GameManager>
{
    public GAME_STATE currentState;
    public GAME_STATE previousState;
    public Profile playerProfile;
    public GameObject CanvasUI_LoadingScreen;
    
    void Start()
    {
        SoundManager.Instance.SetMusicMode(true);
       // SoundManager.SharedInstance.PlayMusic(SOUND_CLIPS.INGAME_MUSIC, transform, 0.01f);
        Init();
    }

    void Init()
    {
        Messenger.AddListener<string>(GAME_EVENTS.CRYSTAL_FOUND.ToString(), OnCrystalFound);
		Messenger.AddListener<DynamicCharacterAvatar>(GAME_EVENTS.SAVE_PLAYER_AVATAR.ToString (), SavePlayeAvatar);
		Messenger.AddListener<DynamicCharacterAvatar>(GAME_EVENTS.LOAD_PLAYER_AVATAR.ToString (), LoadPlayerAvatar);

        playerProfile = new Profile("" + SystemInfo.deviceUniqueIdentifier);
        playerProfile.MaxXPLevel = 10;
        playerProfile.MaxHPLevel = 10;
        playerProfile.XPLevel = 0;
        playerProfile.HPLevel = playerProfile.MaxHPLevel;
   
        //ScoreManager.SharedInstance.Init ();
        SetCurrentStateTo(currentState);
    }

    void OnCrystalFound(string crystalId)
    {
        CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.CRYSTAL_KNOWLEDGE);//Opens Knowledge UIScreen
    }

    public void IncreaseEnergy(int amount)
    {
        playerProfile.TotalEnergy+= amount;
        Messenger.Broadcast("OnPlayerDataUpdated");
    }
    public void DecreaseEnergy(int amount)
    {
        playerProfile.TotalEnergy-= amount;
        Messenger.Broadcast("OnPlayerDataUpdated");
    }
    public int GetEnergy()
    {
        return playerProfile.TotalEnergy;
    }
    public void SetCurrentStateTo(GAME_STATE state)
    {
        previousState = currentState;
        currentState = state;
        switch (currentState)
        {
            case GAME_STATE.IN_GAME:
                CanvasManager.Instance.SetCurrentScreenTo (CANVAS_SCREEN.PLAYER_HUD);
                CameraManager.Instance.SetCameraMode(CameraManager.Mode.Follow);
                break;
            case GAME_STATE.BATTLE:
                CameraManager.Instance.SetCameraMode(CameraManager.Mode.Battle);
                break;

        }
    }

    public void OnVideoAddCompleteEvent()
    {

    }

    public void LoadSceneAndSetCurrentStateTO(string SceneName, GAME_STATE state, LoadSceneMode mode)
    {
        //if (CanvasUI_LoadingScreen)
        //{
        //    CanvasUI_LoadingScreen.SetActive(true);

        //}
        StartCoroutine(LoadScene(SceneName, state, mode));
    }

    //public void LoadScene(string SceneName)
    //{
    //    SceneManager.LoadScene(SceneName, LoadSceneMode.Single);
    //}

    IEnumerator LoadScene(string SceneName, GAME_STATE state, LoadSceneMode mode)
    {

        SceneManager.LoadScene(SceneName,mode);
        while (!SceneManager.GetSceneByName(SceneName).isLoaded)
        {
            yield return new WaitForSeconds(0);
        }
        SetCurrentStateTo(state);
    }

    public void SetTimeScale(float value)
    {
        Time.timeScale = value;
    }


	void SavePlayeAvatar(DynamicCharacterAvatar _avatar){

		playerProfile.AvatarRecipeData = _avatar.GetCurrentRecipe ();
		playerProfile.AvatarColorsRecipeData = _avatar.GetCurrentColorsRecipe ();

	}
	public void LoadPlayerAvatar(DynamicCharacterAvatar _avatar){

		// Need to maintain order in which data is loaded
		//1:AvatarColorsRecipeData
		//2:AvatarRecipeData

		string Data = playerProfile.AvatarColorsRecipeData;
		if (!string.IsNullOrEmpty (Data))
		{			

			_avatar.LoadFromRecipeString (Data);
		}
		Data = playerProfile.AvatarRecipeData;
		if (!string.IsNullOrEmpty (Data))
		{			

			_avatar.LoadFromRecipeString (Data);
		}


	}
}
