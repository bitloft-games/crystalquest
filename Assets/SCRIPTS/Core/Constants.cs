﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;



public static class Constants
{
    public static int SCREEN_WIDTH = 960;//Screen Resolution
    public static int SCREEN_HEIGHT = 600;//Screen Resolution

    public static WaitForSeconds waitSec_0 = new WaitForSeconds(0);
    public static WaitForSeconds waitSec_1 = new WaitForSeconds(1.0f);
    public static WaitForSeconds nextEffect_waitSec = new WaitForSeconds(0.15f);
    public static WaitForSeconds NextPopup_waitSec = new WaitForSeconds(0.15f);
    public static WaitForSeconds counterAttack_waitSec = new WaitForSeconds(2.0f);

    public static float playerLevelModifier = 0.1f;

    public static float enemyLevelModifier = 0.1f;

    public static int crystalMaxXPLevel = 8;//Max Value For crystalXP

    public static int crystalMaxHeatLevel = 5;//Max HeatLevel For crystalXP


    public static int crystalHeatCoolDownTime = 30;// in sec Cooling time for crystal(When it gets heated)

    public static int crystalLockedTimer = 30;//in sec; Timer for crystal Lock(when Player gives wrong answer)

    public static float playerEnemyBattleDistance = 3.0f;//Dist. between Player_Alien for BattleMode

    public static float playerBossAlienBattleDistance = 2.0f;//Dist. between Player_BossAlien for BattleMode

    public static float ProtalAreaDistance = 10.0f;

    public static Color32 playerDamageValueColor = new Color32(255, 58, 0, 255);//Color for DamageValues of Player

    public static Color32 playerGuardDamageValueColor = new Color32(255, 170, 0, 255);//Color for GuardDamageValues of Player

    public static Color32 enemyDamageValueColor = new Color32(255, 248, 0, 255);//Color for DamageValues of enemy

    public static Color32 BattleModeUIWhiteColor = new Color32(255, 255, 255, 255);//TextColor When Player is in Enemy Range

    public static Color32 BattleModeUILightColor = new Color32(178, 0, 0, 255);//LightRedColor When Player is in Enemy Range

    public static Color32 BattleModeUIDarkColor = new Color32(60, 0, 0, 255);//DarkRedColor When Player is in Enemy Range

    public static Color32 NormalModeUILightColor = new Color32(38, 221, 251, 255);//LightBlueColor for NormalMode

    public static Color32 NormalModeUIDarkColor = new Color32(17, 61, 102, 255);//DarkBlueColor for NormalMode

    public static Color32 YOU_WON_Color = new Color32(40, 192, 218, 255);//You won this battle text color

    public static Color32 Coloratoms_1 = new Color32(0, 240, 255, 255);//color for crystal with id atoms_1

    public static Color32 Coloratoms_2 = new Color32(0, 240, 78, 255);//color for crystal with id atoms_2

    public static Color32 Coloratoms_3 = new Color32(255, 216, 0, 255);//color for crystal with id atoms_3

    public static Color32 Coloratoms_4 = new Color32(255, 0, 252, 255);//color for crystal with id atoms_4

    public static Color32 Coloratoms_5 = new Color32(0, 108, 255, 255);//color for crystal with id atoms_5

    public static int EnemyMaxHealthCap = 1000;

    public static float IntroInstructionCloseDelay = 2.0f; // To close alex gameplay Intro instruction box 

    public static float EnemyDissolveEffectSpeed = 1.2f;

    public static float BOSS_CRYSTAL_MIN_ANSWER_LIMIT = 2.0f;

    public static int AlienNearSoundDistance = 10;
}



