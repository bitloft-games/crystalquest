%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Mask_Enemy_Hanshi_1
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: base_mesh
    m_Weight: 1
  - m_Path: enemy
    m_Weight: 1
  - m_Path: enemy/cog
    m_Weight: 1
  - m_Path: enemy/cog/back_cloth_a
    m_Weight: 1
  - m_Path: enemy/cog/back_cloth_a/back_cloth_a 1
    m_Weight: 1
  - m_Path: enemy/cog/front_cloth_a
    m_Weight: 1
  - m_Path: enemy/cog/front_cloth_a/front_cloth_b
    m_Weight: 1
  - m_Path: enemy/cog/left_cloth_a
    m_Weight: 1
  - m_Path: enemy/cog/left_cloth_a/left_cloth_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/head
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/head/Bip001 HeadNub
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_index_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_index_a/left_index_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_middle_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_middle_a/left_middle_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_thumb_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/left_thumb_a/left_thumb_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/weapon_left
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/weapon_left/weapon_root
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/weapon_left/weapon_root/weapon_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/left_clavicle/left_upper_arm/left_forearm/left_hand/weapon_left/weapon_root/weapon_a/Shord
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_index_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_index_a/right_index_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_middle_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_middle_a/right_middle_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_thumb_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/right_thumb_a/right_thumb_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/Bip001 Neck/right_clavicle/right_upper_arm/right_forearm/right_hand/weapon_right
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/drape_a
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/drape_a/ drape_b
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/chest/drape_a/ drape_b/drape_c
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/left_thigh
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/left_thigh/left_calf
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/left_thigh/left_calf/left_foot
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/left_thigh/left_calf/left_foot/left_toe
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/right_thigh
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/right_thigh/right_calf
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/right_thigh/right_calf/right_foot
    m_Weight: 1
  - m_Path: enemy/cog/pelvis/spine/right_thigh/right_calf/right_foot/right_toe
    m_Weight: 1
  - m_Path: enemy/cog/right_cloth_a
    m_Weight: 1
  - m_Path: enemy/cog/right_cloth_a/right_cloth_b
    m_Weight: 1
