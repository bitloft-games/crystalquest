{
  "crystalCardData":[
    {
      "id": "atoms_2",
      "cardTitle": "Elements",
      "cardDescription": "The Species of Atoms",
      "crystalKnowledge": "A water molecule is composed of 2 hydrogen atoms and one water atom",
      "crystalXPLevel" : "0",
      "crystalXPMultiplier":"2",
      "crystalLevel" : "1",
      "crystalLevelMultiplier":"1",
      "crystalHeatLevel": "0",
      "crystalHeatMultiplier":"1",
      "crystalMinDamageValue":"1",
      "crystalMaxDamageValue":"10",
      "questions": [
        	{

         	 "question": "An atom that has 7 protons, 7 neutrons, and 7 electrons would have an atomic mass of _____.",
         	 "options": [
           	 {
              "option": "7"
              
           	 },
           	 {
              "option": "14"
            
           	 },
           	 {
              "option": "21"
             
           	 },
           	 {
              "option": "28"
             
           	 }
         	 ],
         	 "answer": "14"
        	},
       	 	{
        	"question": "An atom that has 8 protons, 8 neutrons, and 8 electrons would have an atomic number of ____.",
         	 "options": [
           	 {
              "option": "8"
              
           	 },
           	 {
              "option": "16"
            
             },
           	 {
              "option": "24"
             
           	 },
          	 {
              "option": "4"
             
             }
         	 ],
         	 "answer": "8"
       		},
       		{
        	"question": "The atomic number is equal to the number of ______ within the nucleus of an atom.",
         	 "options": [
           	 {
              "option": "Electrons"
              
           	 },
           	 {
              "option": "Protons"
            
             },
           	 {
              "option": "Neutrons"
             
           	 },
          	 {
              "option": "Quarks"
             
             }
         	 ],
         	 "answer": "Protons"
       		},
       		{
        	"question": "A carbon atom has 6 electrons, 6 protons, and 8 neutrons. What is the atomic mass?",
         	 "options": [
           	 {
              "option": "6"
              
           	 },
           	 {
              "option": "8"
            
             },
           	 {
              "option": "12"
             
           	 },
          	 {
              "option": "14"
             
             }
         	 ],
         	 "answer": "14"
       		},
       		{
        	"question": "An atom that has more protons than electrons is known as a ______.",
         	 "options": [
           	 {
              "option": "Cation"
              
           	 },
           	 {
              "option": "Anion"
            
             },
           	 {
              "option": "Neutral atom"
             
           	 },
          	 {
              "option": "Isotope"
             
             }
         	 ],
         	 "answer": "Cation"
       		},
       		{
        	"question": "An atom that has more electrons than protons is known as a ______.",
         	 "options": [
           	 {
              "option": "Cation"
              
           	 },
           	 {
              "option": "Anion"
            
             },
           	 {
              "option": "Neutral atom"
             
           	 },
          	 {
              "option": "Isotope"
             
             }
         	 ],
         	 "answer": "Anion"
       		},
       		{
        	"question": "Atoms that have the same number of protons but different numbers of neutrons are known as _____.",
         	 "options": [
           	 {
              "option": "Ions"
              
           	 },
           	 {
              "option": "Base Pairs"
            
             },
           	 {
              "option": "Isotopes"
             
           	 },
          	 {
              "option": "Cations"
             
             }
         	 ],
         	 "answer": "Isotopes"
       		},
       		{
        	"question": "To discover an elements atomic mass, you must ______.",
         	 "options": [
           	 {
              "option": "Subtract the number of protons by the number of neutrons"
              
           	 },
           	 {
              "option": "Multiply the number of protons by the number of electrons"
            
             },
           	 {
              "option": "Divide the number of electrons by the number of protons"
             
           	 },
          	 {
              "option": "Add together the number of protons with the number of neutrons"
             
             }
         	 ],
         	 "answer": "Add together the number of protons with the number of neutrons"
       		},
       		{
        	"question": "If an atom loses an electron, it becomes a(n) _______.",
         	 "options": [
           	 {
              "option": "Anion"
              
           	 },
           	 {
              "option": "Cation"
            
             },
           	 {
              "option": "Isotope"
             
           	 },
          	 {
              "option": "Monomer"
             
             }
         	 ],
         	 "answer": "Cation"
       		},
       		{
        	"question": "When two atoms are isotopes of one another that means ______.",
         	 "options": [
           	 {
              "option": "Their number of protons differ"
              
           	 },
           	 {
              "option": "Their number of electrons differ"
            
             },
           	 {
              "option": "Their number of neutrons differ"
             
           	 },
          	 {
              "option": "They are identical"
             
             }
         	 ],
         	 "answer": "Their number of neutrons differ"
       		},
       		{
        	"question": "Two isotopes of the same element differ in their number of _____.",
         	 "options": [
           	 {
              "option": "Protons"
              
           	 },
           	 {
              "option": "Neutrons"
            
             },
           	 {
              "option": "Electrons"
             
           	 },
          	 {
              "option": "Bonds"
             
             }
         	 ],
         	 "answer": "Neutrons"
       		},
       		{
        	"question": "What is the chemical symbol for Carbon?",
         	 "options": [
           	 {
              "option": "O"
              
           	 },
           	 {
              "option": "C"
            
             },
           	 {
              "option": "H"
             
           	 },
          	 {
              "option": "Ca"
             
             }
         	 ],
         	 "answer": "C"
       		},
       		{
        	"question": "What is the chemical symbol for Oxygen?",
         	 "options": [
           	 {
              "option": "C"
              
           	 },
           	 {
              "option": "N"
            
             },
           	 {
              "option": "H"
             
           	 },
          	 {
              "option": "O"
             
             }
         	 ],
         	 "answer": "O"
       		},
       		{
        	"question": "An atom that has lost or gained electrons is known as a(n) ________",
         	 "options": [
           	 {
              "option": "Proton"
              
           	 },
           	 {
              "option": "Isotope"
            
             },
           	 {
              "option": "Ion"
             
           	 },
          	 {
              "option": "Neutron"
             
             }
         	 ],
         	 "answer": "Ion"
       		}
      ]
    },
    {
      "id": "atoms_1",
      "cardTitle": "Atomic Structure",
      "cardDescription": "The components of an atom",
      "crystalKnowledge": "crystalKnowledge_2",
      "crystalXPLevel" : "0",
       "crystalXPMultiplier":"2",
      "crystalLevel" : "1",
      "crystalLevelMultiplier":"1",
      "crystalHeatLevel": "0",
       "crystalHeatMultiplier":"1",
      "crystalMinDamageValue":"1",
      "crystalMaxDamageValue":"10",
      "questions": [
        {
          "question": "What are atoms made up of?",
          "options": [
            {
              "option": "Protons, Neutrons, & Isotrons"
              
            },
            {
              "option": "Protons, Neutrons, & Electrons"
            
            },
            {
              "option": "Protons, Electrons, & Aminotrons"
             
            },
            {
              "option": "Electrons, Aminotrons, & Isotrons"
             
            }
          ],
          "answer": "Protons, Neutrons, & Electrons"
        },
        {
          "question": "Which atomic particle has a positive charge?",
          "options": [
            {
              "option": "Proton"
              
            },
            {
              "option": "Neutron"
            
            },
            {
              "option": "Electron"
             
            },
            {
              "option": "Negatron"
             
            }
          ],
          "answer": "Proton"
        },
        {
          "question": "Which atomic particle has a neutral charge?",
          "options": [
            {
              "option": "Proton"
              
            },
            {
              "option": "Neutron"
            
            },
            {
              "option": "Electron"
             
            },
            {
              "option": "Negatron"
             
            }
          ],
          "answer": "Neutron"
        },
        {
          "question": "Which atomic particle has a negative charge?",
          "options": [
            {
              "option": "Proton"
              
            },
            {
              "option": "Neutron"
            
            },
            {
              "option": "Electron"
             
            },
            {
              "option": "Negatron"
             
            }
          ],
          "answer": "Electron"
        },
        {
          "question": "Where are protons and neutrons located?",
          "options": [
            {
              "option": "Within the electron cloud"
              
            },
            {
              "option": "Underneath the atomic rug"
            
            },
            {
              "option": "Inside the atomic nucleus"
             
            },
            {
              "option": "Hidden in a secret compartment"
             
            }
          ],
          "answer": "Inside the atomic nucleus"
        },
        {
          "question": "Where are electrons located?",
          "options": [
            {
              "option": "Buzzing around the electron cloud"
              
            },
            {
              "option": "Inside the atomic nucleus"
            
            },
            {
              "option": "In between protons"
             
            },
            {
              "option": "Within the atomic mailbox"
             
            }
          ],
          "answer": "Buzzing around the electron cloud"
        },
        {
          "question": "Electrons have a _____ charge.",
          "options": [
            {
              "option": "Positive"
              
            },
            {
              "option": "Neutral"
            
            },
            {
              "option": "Forward"
             
            },
            {
              "option": "Negative"
             
            }
          ],
          "answer": "Negative"
        },
        {
          "question": "Protons have a _____ charge.",
          "options": [
            {
              "option": "Positive"
              
            },
            {
              "option": "Negative"
            
            },
            {
              "option": "Neutral"
             
            },
            {
              "option": "Federal"
             
            }
          ],
          "answer": "Positive"
        },
        {
          "question": "Neutrons have a ______ charge.",
          "options": [
            {
              "option": "Positive"
              
            },
            {
              "option": "Negative"
            
            },
            {
              "option": "Neutral"
             
            },
            {
              "option": "Undercover"
             
            }
          ],
          "answer": "Neutral"
        },
        {
          "question": "What is the atomic mass (AMU) of a neutron?",
          "options": [
            {
              "option": "0"
              
            },
            {
              "option": "1"
            
            },
            {
              "option": "2"
             
            },
            {
              "option": "3"
             
            }
          ],
          "answer": "1"
        },
        {
          "question": "What is the atomic mass (AMU) of a proton?",
          "options": [
            {
              "option": "0"
              
            },
            {
              "option": "1"
            
            },
            {
              "option": "2"
             
            },
            {
              "option": "3"
             
            }
          ],
          "answer": "1"
        }
      ]
    },
    {
      "id": "atoms_3",
      "cardTitle": "Chemical Bonds",
      "cardDescription": "cardDescription_3",
      "crystalKnowledge": "crystalKnowledge_3",
     "crystalXPLevel" : "0",
      "crystalXPMultiplier":"2",
      "crystalLevel" : "1",
      "crystalLevelMultiplier":"1",
      "crystalHeatLevel": "0",
       "crystalHeatMultiplier":"1",
     "crystalMinDamageValue":"1",
      "crystalMaxDamageValue":"10",
      "questions": [
        {
          "question": "Which atomic particle plays the most important role in atomic chemistry?",
          "options": [
            {
              "option": "Protons"
              
            },
            {
              "option": "Neutrons"
            
            },
            {
              "option": "Electrons"
             
            },
            {
              "option": "Neutrinos"
             
            }
          ],
          "answer": "Electrons"
        },
        {
          "question": "Which bond involves the sharing of outer (valence) electrons?",
          "options": [
            {
              "option": "Covalent Bond"
              
            },
            {
              "option": "Ionic Bond"
            
            },
            {
              "option": "Hydrogen Bond"
             
            },
            {
              "option": "Jimmy Bond"
             
            }
          ],
          "answer": "Covalent Bond"
        },
        {
          "question": "Which bond forms because of the attraction between oppositely-charged ions?",
          "options": [
            {
              "option": "Covalent Bond"
              
            },
            {
              "option": "Ionic Bond"
            
            },
            {
              "option": "Hydrogen Bond"
             
            },
            {
              "option": "Adhesive Bond"
             
            }
          ],
          "answer": "Ionic Bond"
        },
        {
          "question": "Which bond forms between hydrogen atoms of separate water molecules?",
          "options": [
            {
              "option": "Covalent Bond"
              
            },
            {
              "option": "Ionic Bond"
            
            },
            {
              "option": "Hydrogen Bond"
             
            },
            {
              "option": "Cohesive Bond"
             
            }
          ],
          "answer": "Hydrogen Bond"
        },
        {
          "question": "Which answer best describes ionic bonds?",
          "options": [
            {
              "option": "Protons are shared equally between the atoms"
              
            },
            {
              "option": "Electrons are shared equally between the atoms"
            
            },
            {
              "option": "Electrons are shared unequally between the atoms"
             
            },
            {
              "option": "Electrons are transferred between atoms"
             
            }
          ],
          "answer": "Electrons are transferred between atoms"
        },
        {
          "question": "Which of the following bonds are the strongest and most stable?",
          "options": [
            {
              "option": "Ionic Bonds"
              
            },
            {
              "option": "Hydrogen Bonds"
            
            },
            {
              "option": "Covalent Bonds"
             
            },
            {
              "option": "Berry Bonds"
             
            }
          ],
          "answer": "Ionic Bonds"
        },
        {
          "question": "Covalent bonds form between _____ & _____.",
          "options": [
            {
              "option": "Metals, Metals"
              
            },
            {
              "option": "Metals, Non-Metals"
            
            },
            {
              "option": "Non-Metals, Non-Metals"
             
            },
            {
              "option": "Salts, Sugars"
             
            }
          ],
          "answer": "Non-Metals, Non-Metals"
        },
        {
          "question": "Atoms in a molecule are held together by _________.",
          "options": [
            {
              "option": "Chemical Bonds"
              
            },
            {
              "option": "Super Glue"
            
            },
            {
              "option": "Gravity"
             
            },
            {
              "option": "Thermonuclear Forces"
             
            }
          ],
          "answer": "Chemical Bonds"
        }
      ]
    },
    {
      "id": "atoms_4",
       "cardTitle": "Atoms",
      "cardDescription": "cardDescription_4",
      "crystalKnowledge": "crystalKnowledge_4",
      "crystalXPLevel" : "0",
       "crystalXPMultiplier":"2",
      "crystalLevel" : "1",
      "crystalLevelMultiplier":"1",
      "crystalHeatLevel": "0",
       "crystalHeatMultiplier":"1",
     "crystalMinDamageValue":"1",
      "crystalMaxDamageValue":"10",
      "questions": [
        {
          "question": "Who developed the atomic theory?",
          "options": [
            {
              "option": "Charles Darwin"
              
            },
            {
              "option": "Albert Einstein"
            
            },
            {
              "option": "John Dalton"
             
            },
            {
              "option": "Nikola Tesla"
             
            }
          ],
          "answer": "John Dalton"
        },
        {
          "question": "What is the definition of “Matter”?",
          "options": [
            {
              "option": "A type of ion"
              
            },
            {
              "option": "Anything that occupies space and has mass."
            
            },
            {
              "option": "Rocks and dirt"
             
            },
            {
              "option": "Anything that can conduct electricity"
             
            }
          ],
          "answer": "Anything that occupies space and has mass."
        },
        {
          "question": "All matter is made up out of _____.",
          "options": [
            {
              "option": "Cells"
              
            },
            {
              "option": "Atoms"
            
            },
            {
              "option": "Space Dust"
             
            },
            {
              "option": "Putty"
             
            }
          ],
          "answer": "Atoms"
        },
        {
          "question": "Which state of matter has a definite shape and definite volume?",
          "options": [
            {
              "option": "Liquid"
              
            },
            {
              "option": "Solid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Solid"
        },
        {
          "question": "Which state of matter has indefinite shape but definite volume?",
          "options": [
            {
              "option": "Liquid"
              
            },
            {
              "option": "Solid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Liquid"
        },
        {
          "question": "Which state of matter readily and actively conducts electricity?",
          "options": [
            {
              "option": "Solid"
              
            },
            {
              "option": "Liquid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Plasma"
        },
        {
          "question": "Which state of matter has particles that are tightly compacted and organized?",
          "options": [
            {
              "option": "Solid"
              
            },
            {
              "option": "Liquid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Solid"
        },
        {
          "question": "Which state of matter has particles that are close together, but can flow over one another?",
          "options": [
            {
              "option": "Solid"
              
            },
            {
              "option": "Liquid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Liquid"
        },
        {
          "question": "Which state of matter has neutrons floating through a “sea” of electrons?",
          "options": [
            {
              "option": "Solid"
              
            },
            {
              "option": "Liquid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Plasma"
        },
        {
          "question": "Which state of matter has particles that fly around at high speed in random directions?",
          "options": [
            {
              "option": "Solid"
              
            },
            {
              "option": "Liquid"
            
            },
            {
              "option": "Gas"
             
            },
            {
              "option": "Plasma"
             
            }
          ],
          "answer": "Gas"
        }
      ]
    },
    {
      "id": "atoms_5",
             "cardTitle": "Molecules & Compounds",
      "cardDescription": "cardDescription_5",
      "crystalKnowledge": "crystalKnowledge_5",
    "crystalXPLevel" : "0",
     "crystalXPMultiplier":"2",
      "crystalLevel" : "1",
      "crystalLevelMultiplier":"1",
      "crystalHeatLevel": "0",
       "crystalHeatMultiplier":"1",
     "crystalMinDamageValue":"1",
      "crystalMaxDamageValue":"10",
      "questions": [
        {
          "question": "Are all molecules compounds?",
          "options": [
            {
              "option": "Yes"
              
            },
            {
              "option": "No"
            
            },
            {
              "option": "Maybe"
             
            },
            {
              "option": "I don't know"
             
            }
          ],
          "answer": "No"
        },
        {
          "question": "Are all compounds molecules?",
          "options": [
            {
              "option": "Yes"
              
            },
            {
              "option": "No"
            
            },
            {
              "option": "Maybe"
             
            },
            {
              "option": "I don't know"
             
            }
          ],
          "answer": "Yes"
        },
         {
          "question": "Salt(NaCl) is an example of a(n) _______.",
          "options": [
            {
              "option": "Isotope"
              
            },
            {
              "option": "Molecule"
            
            },
            {
              "option": "Compound"
             
            },
            {
              "option": "Element"
             
            }
          ],
          "answer": "Compound"
        },
        {
          "question": "Oxygen, (O2) is an example of a(n) _______.",
          "options": [
            {
              "option": "Isotope"
              
            },
            {
              "option": "Molecule"
            
            },
            {
              "option": "Hydrogen Bond"
             
            },
            {
              "option": "Compound"
             
            }
          ],
          "answer": "Molecule"
        },
        {
          "question": "When 2+ atoms from different elements combine, a ______ is formed.",
          "options": [
            {
              "option": "Molecule"
              
            },
            {
              "option": "Monomer"
            
            },
            {
              "option": "Compound"
             
            },
            {
              "option": "Concentrate"
             
            }
          ],
          "answer": "Compound"
        }
      ]
    }
    
    
  ]
}