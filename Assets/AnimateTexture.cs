﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateTexture : MonoBehaviour {

    public float moveX = 0.001f;
    public float moveY = 0.001f;

    Material material;

    void Start()
    {
        Renderer rend = gameObject.GetComponent<Renderer>();
        material = rend.material;
    }

	void Update () {
        material.mainTextureOffset+= new Vector2(moveX, moveY);
    }
}
