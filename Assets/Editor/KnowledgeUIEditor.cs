﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

[CustomEditor(typeof(KnowledgeUI))]
[CanEditMultipleObjects]
public class KnowledgeUIEditor : Editor
{

    protected KnowledgeUI ui;

    protected SerializedProperty KnowledgeTextAsset;
    protected SerializedProperty editorText;
    protected SerializedProperty h1TextPrefab;
    protected SerializedProperty h2TextPrefab;
    protected SerializedProperty pTextPrefab;
    protected SerializedProperty imgPrefab;
    protected SerializedProperty liTextPrefab;

    void OnEnable()
    {
        ui = (KnowledgeUI)serializedObject.targetObject;

        KnowledgeTextAsset = serializedObject.FindProperty("KnowledgeTextAsset");
        editorText = serializedObject.FindProperty("editorText");
        h1TextPrefab = serializedObject.FindProperty("h1TextPrefab");
        h2TextPrefab = serializedObject.FindProperty("h2TextPrefab");
        pTextPrefab = serializedObject.FindProperty("pTextPrefab");
        imgPrefab = serializedObject.FindProperty("imgPrefab");
        liTextPrefab = serializedObject.FindProperty("liTextPrefab");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        //set up the editor GUI
        EditorGUILayout.PropertyField(KnowledgeTextAsset, new GUIContent("Text from file"));
        EditorGUILayout.LabelField(new GUIContent("Manually enter text (text file will be ignored)"));
        EditorGUILayout.PropertyField(editorText, new GUIContent(""));
        EditorGUILayout.PropertyField(h1TextPrefab);
        EditorGUILayout.PropertyField(h2TextPrefab);
        EditorGUILayout.PropertyField(pTextPrefab);
        EditorGUILayout.PropertyField(liTextPrefab);
        EditorGUILayout.PropertyField(imgPrefab);
               
        //Update scene GUI
        if (GUILayout.Button("Build GUI"))
        {
            ui.UpdateText();
            // set dirty so the editor knows to update the scene
            UnityEditor.EditorUtility.SetDirty(ui);
        }
        if (GUILayout.Button("Clear GUI"))
        {
            ui.clearChildren();
            UnityEditor.EditorUtility.SetDirty(ui);
        }

        serializedObject.ApplyModifiedProperties();
    }

    public void OnSceneGUI()
    {

    }
    
}
