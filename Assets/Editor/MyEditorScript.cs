﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.IO;

class MyEditorScript {
	
	//static string[] SCENES = FindEnabledEditorScenes();

	static string APP_NAME = "CrystelQuest";
	static string TARGET_DIR = "Builds";
	/*
	[MenuItem ("Custom/CI/Build Mac OS X")]
	static void PerformMacOSXBuild ()
	{
		string target_dir = APP_NAME+".app";
		GenericBuild(SCENES, TARGET_DIR + "/Mac/" + target_dir, BuildTarget.StandaloneOSXIntel,BuildOptions.None);
	}
	[MenuItem ("Custom/CI/Build Linux X")]
	static void PerformLinuxBuild ()
	{
		string target_dir = APP_NAME;;
		GenericBuild(SCENES, TARGET_DIR + "/Linux/" + target_dir, BuildTarget.StandaloneLinux,BuildOptions.None);
	}
	[MenuItem ("Custom/CI/Build Windows X")]
	static void PerformWindowsBuild ()
	{
		string target_dir = APP_NAME+".exe";
		GenericBuild(SCENES, TARGET_DIR + "/Windows/" + target_dir, BuildTarget.StandaloneWindows,BuildOptions.None);
	}

	private static string[] FindEnabledEditorScenes() {
		List<string> EditorScenes = new List<string>();
		foreach(EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (!scene.enabled) continue;
			EditorScenes.Add(scene.path);
		}
		return EditorScenes.ToArray();
	}

	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		string res = BuildPipeline.BuildPlayer(scenes,target_dir,build_target,build_options);
		if (res.Length > 0) {
			throw new Exception("BuildPlayer failure: " + res);
		}
	}
	*/

	[MenuItem("Automation/Build Windows Player")]
	public static void BuildWindowsPlayer() {
		// get the scenes from the settings dialog
		var scenes = EditorBuildSettings.scenes.Select (scene => scene.path).ToArray ();

		var outputDir = Environment.GetFolderPath (Environment.SpecialFolder.Desktop);

		BuildPipeline.BuildPlayer (scenes, outputDir + "/"+APP_NAME+".exe",
			BuildTarget.StandaloneWindows,
			BuildOptions.None
		);
	}
	// Helper function for getting the command line arguments
	private static string GetArg(string name)
	{
		var args = System.Environment.GetCommandLineArgs();
		for (int i = 0; i < args.Length; i++)
		{
			if (args[i] == name && args.Length > i + 1)
			{
				return args[i + 1];
			}
		}
		return null;
	}

	// This actually builds a windows player.
	private static void DoBuild(string outputDir,string buildName,BuildTarget target) {
		if (outputDir == null) {
			throw new ArgumentException("No output folder specified.");
		}

		if (!Directory.Exists (outputDir)) {
			Directory.CreateDirectory (outputDir);
		}

		// get the scenes from the settings dialog
		var scenes = EditorBuildSettings.scenes.Select (scene => scene.path).ToArray ();

		BuildPipeline.BuildPlayer (scenes, outputDir + "/"+buildName,
			target,
			BuildOptions.None
		);

	}

	// This is called by the editor menu.
	[MenuItem("Automation/Build Windows Player")]
	public static void BuildWindowsPlayerInEditor() {
		// in the editor we just put the player on the desktop
		DoBuild(Environment.GetFolderPath (Environment.SpecialFolder.Desktop),APP_NAME+".exe",BuildTarget.StandaloneWindows);
	}
	// this is called by the CI server
	public static void BuildWindowsPlayerInCI() {
		// read the "-outputDir" command line argument
		var outputDir = GetArg ("-outputDir");

		// use the existing code to build the player
		DoBuild (outputDir,APP_NAME+".exe",BuildTarget.StandaloneWindows);

		// end the application when done.
		EditorApplication.Exit (0);
	}
	//For Mac Build
	public static void BuildMacPlayerInCI() {
		// read the "-outputDir" command line argument
		var outputDir = GetArg ("-outputDir");

		// use the existing code to build the player
		DoBuild (outputDir,APP_NAME+".app",BuildTarget.StandaloneOSXIntel);

		// end the application when done.
		EditorApplication.Exit (0);
	}
	//For Linux Build
	public static void BuildLinuxPlayerInCI() {
		// read the "-outputDir" command line argument
		var outputDir = GetArg ("-outputDir");

		// use the existing code to build the player
		DoBuild (outputDir,APP_NAME+"",BuildTarget.StandaloneLinux);

		// end the application when done.
		EditorApplication.Exit (0);
	}
}