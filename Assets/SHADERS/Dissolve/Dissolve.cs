﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissolve : MonoBehaviour {

	public Renderer rend;
	void Start() {
		rend = GetComponent<Renderer>();

	}
	public float dissolveValue;
	void Update() {
		
		rend.material.SetFloat("_SliceAmount", dissolveValue);
	}
}
