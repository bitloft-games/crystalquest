﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyGate : MonoBehaviour {

    public enum GateState
    {
        Open,
        Close
    }
    public int energyCost;
    public GameObject openObject;
    public GameObject closedObject;
    public GameObject openEffect;

    public GateState gateState = GateState.Close;
    
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnterAttempt()
    {
        if (gateState == GateState.Close)
        {
            if (GameManager.Instance.GetEnergy() >= energyCost)
            {
                CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.YESNO);
                CanvasUI_YesNo yesno = GameObject.FindObjectOfType<CanvasUI_YesNo>();
                yesno.Setup("Pay " + energyCost.ToString() + " Energy to open forcefield?", "Pay "+ energyCost.ToString()+" Energy", Pay, "No");
            } else
            {
                CanvasManager.Instance.SetCurrentScreenTo(CANVAS_SCREEN.NOTIFICATION);
                CanvasUI_Notification notif = GameObject.FindObjectOfType<CanvasUI_Notification>();
                notif.Setup("Not enough energy. Energy can be found in Knowledge crystals.");

            }
        }
    }

    public void Pay()
    {
        GameManager.Instance.DecreaseEnergy(energyCost);
        gateState = GateState.Open;
        closedObject.SetActive(false);
        BoxCollider boxCollider = gameObject.GetComponent<BoxCollider>();
        boxCollider.enabled = false;
        if (openObject != null) openObject.SetActive(true);
        if (openEffect != null) openEffect.SetActive(true);
        
    }

    public void Close()
    {
        gateState = GateState.Close;
    }
    
}
