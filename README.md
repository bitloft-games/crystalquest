## Crystal Quest

### Setup instructions
TODO

### Build instructions
TODO

### How to contribute
- clone the repository locally  
`git clone git@bitbucket.org:asimplegamestudio/crystalquest.git`
- Change to the develop branch  
`git checkout develop`
- Create a new branch with the name of the bug/feature you plan to make. You can use the bug/feature number or just try to make a really short descriptive branchname.   
`git checkout -b bugfix-1234` or `git checkout -b bugfix-player-walk`
- Make your changes locally, test it thoroughly, then prep your commit.  
`git status` - look at what you have changed and remove stuff you don't need anymore if you have to.  
`git add <filename to add>` - this queus the file up to your commit in progress  
`git commit -m "Fix walking animation"` - Make the verb present-tense and create your commit.  
`git push origin bugfix-player-walk` - push your changes up to the repository  
- Take a look at your branch on bitbucket and create a pull request from develop.
- Ask for someone to take a look at the pull request and approve it.
- If you get approval, feel free to merge back into develop.
- Once ready for a new release, and the develop branch has been really really really tested. Make a pull request going from develop to master and merge. Tag that version with the [semantic versioning](http://semver.org/): MAJOR.MINOR.PATCH

#### Problems?
 - Make sure your bitbucket account has your ssh key. Check your settings to make sure.
 - If you have a merge conflict, someone might have pushed to develop before you did and it conflicts with the code you're trying to merge. You'll want to resolve those conflicts with...  
 `git pull --rebase origin develop` - this puts your changes on top of their changes. It makes for a cleaner commit history than `git pull origin develop`

- Make your merging of unity files super easy! Use their gitmerge tool.
 Set up Unity Merge Tool using [these instructions](http://werc.iridia.fr/Blog/2016/02/11/0/) and this [diff tool](https://sourcegear.com/diffmerge/downloads.php).  
Add this line to both unity and prefab specs in c:/Program Files/Unity/Editor/Data/Tools/mergespecfile.txt to make diffmerge your fallback mergetool    
```use "%programs%\SourceGear\Common\DiffMerge\sgdm.exe" --nosplash -m -t1="Incoming Changes" -t2="Base" -t3="Working Copy" -r="%d" "%l" "%b" "%r"
```

 Then when you run into a merge conflict for a unity file, use  
 `git mergetool --tool=unityyamlmerge` (if you named the tool unityyamlmerge)
